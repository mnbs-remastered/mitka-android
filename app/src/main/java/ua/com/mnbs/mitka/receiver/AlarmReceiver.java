package ua.com.mnbs.mitka.receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.util.List;
import java.util.Random;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.service.NotificationsService;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "AlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences preferences = context.getSharedPreferences("reminders", Context.MODE_PRIVATE);
        if (!preferences.getBoolean("enabled", true)) {
            return;
        }
        Bundle bundle = getResultExtras(true);
        int action = Integer.parseInt(intent.getAction());
        String name = intent.getStringExtra("name");
        List<Mitka> mitkas = intent.getParcelableArrayListExtra("mitkas");
        Mitka currentMitka = intent.getParcelableExtra("mitka");

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "reminders";
        NotificationCompat.Builder notification;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Mitka!", importance);
            notificationManager.createNotificationChannel(channel);
        }

        notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentTitle("Нагадування")
                //.setContentText(name)
                .setSmallIcon(R.mipmap.ic_launcher);
        notificationManager.notify(new Random().nextInt(), notification.build());
/*
        if (currentMitka == null ) {
            for (Mitka mitka : mitkas) {
               beepMitka(mitka, context);
            }
        } else {
            beepMitka(currentMitka, context);
        }*/
    }


    private void beepMitka(Mitka mitka, Context context) {
        try {
            mitka.connect(context);
            mitka.beep();
        } catch (InterruptedException e) {
            Log.e(TAG, "connection error");
        }
    }
}
