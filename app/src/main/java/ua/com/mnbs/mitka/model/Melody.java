package ua.com.mnbs.mitka.model;

public enum Melody {
    MELODY1("Star Wars", 1),
    MELODY2("Astronomia", 2),
    MELODY3("Take on Me", 3);

    private String text;
    private int number;

    Melody(String text, int number) {
        this.text = text;
        this.number = number;
    }

    @Override
    public String toString() {
        return text;
    }

    public int getNumber() {
        return number;
    }
}
