package ua.com.mnbs.mitka.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.model.Mitka;

public class MitkasPackAdapter extends RecyclerView.Adapter<MitkasPackAdapter.ViewHolder> {

    private Context context;
    private List<Mitka> mitkas;
    public MitkasPackAdapter(Context context, List<Mitka> mitkas, boolean isEditable) {
        if (isEditable) {
            mitkas.add(0, new Mitka());
        }
        this.context = context;
        this.mitkas = mitkas;
    }

    @NonNull
    @Override
    public MitkasPackAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_pack_mitka, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MitkasPackAdapter.ViewHolder holder, int position) {
        Mitka currentMitka = mitkas.get(position);

        holder.mitkaImageView.setImageResource(currentMitka.getMitkaImage());
        holder.mitkaNameTextView.setText(currentMitka.getName());
    }

    @Override
    public int getItemCount() {
        return mitkas.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mitkaImageView;
        private TextView mitkaNameTextView;

        public ViewHolder(@NonNull View view) {
            super(view);
            mitkaImageView = view.findViewById(R.id.pack_mitka_image);
            mitkaNameTextView = view.findViewById(R.id.pack_mitka_name);
        }
    }
}
