package ua.com.mnbs.mitka.model;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.service.BluetoothLeService;

public class Mitka implements Serializable, Parcelable {

    private static final String UUID_TO_WRITE = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
    private static final String UUID_TO_READ = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
    protected static final UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private boolean isConnected;

    private int mitkaImage;
    private String name;
    private ConnectionStatus status;
    private boolean visible;
    private String ownerId;
    private int signalPower;
    private GeoPoint location;
    private int batteryPower;
    private String macAddress;
    private BluetoothLeService bluetoothLeService = new BluetoothLeService();
    private BluetoothGattCharacteristic characteristic;
    private Context context;
    private Timestamp lastSeen;
    private List<Pack> packs;
    private String melodyFileForPhone;
    private int melody;

    public Mitka() {
    }

    public Mitka(int mitkaImage, String name, int signalPower, int batteryPower) {
        this.mitkaImage = mitkaImage;
        this.name = name;
        this.signalPower = signalPower;
        this.batteryPower = batteryPower;
    }

    public Mitka(int mitkaImage, String name, GeoPoint location, String macAddress, Timestamp lastSeen, List<Pack> packs) {
        this.mitkaImage = mitkaImage;
        this.name = name;
        this.location = location;
        this.macAddress = macAddress;
        this.lastSeen = lastSeen;
        this.packs = packs;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", this.name);
        map.put("mitkaImage", this.mitkaImage);
        map.put("location", this.location);
        map.put("macAddress", this.macAddress);
        map.put("lastSeen", this.lastSeen);
        map.put("packs", this.packs);
        map.put("melody", this.melody);
        return map;
    }

    public Mitka(String macAddress, int signalPower) {
        this.macAddress = macAddress;
        this.signalPower = signalPower;
    }

    protected Mitka(Parcel in) {
        mitkaImage = in.readInt();
        batteryPower = in.readInt();
        name = in.readString();
        visible = in.readByte() != 0;
        ownerId = in.readString();
        signalPower = in.readInt();
        batteryPower = in.readInt();
        macAddress = in.readString();
        location = new GeoPoint(in.readDouble(), in.readDouble());
        in.readTypedList(packs, Pack.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mitkaImage);
        dest.writeInt(batteryPower);
        dest.writeString(name);
        if (visible) {
            dest.writeByte((byte) 1);
        } else {
            dest.writeByte((byte) 0);
        }
        dest.writeString(ownerId);
        dest.writeInt(signalPower);
        dest.writeInt(batteryPower);
        dest.writeString(macAddress);
        if (location != null) {
            dest.writeDouble(location.getLatitude());
            dest.writeDouble(location.getLongitude());
        } else {
            dest.writeDouble(0);
            dest.writeDouble(0);
        }
        dest.writeTypedList(packs);
    }

    public static final Creator<Mitka> CREATOR = new Creator<Mitka>() {
        @Override
        public Mitka createFromParcel(Parcel in) {
            return new Mitka(in);
        }

        @Override
        public Mitka[] newArray(int size) {
            return new Mitka[size];
        }
    };

    public int getMitkaImage() {
        return mitkaImage;
    }

    public void setMitkaImage(int mitkaImage) {
        this.mitkaImage = mitkaImage;
    }

    public ConnectionStatus getStatus() {
        return status;
    }

    public void setStatus(ConnectionStatus status) {
        this.status = status;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSignalPower() {
        return signalPower;
    }

    public void setSignalPower(int signalPower) {
        this.signalPower = signalPower;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Timestamp getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Timestamp lastSeen) {
        this.lastSeen = lastSeen;
    }

    public int getBatteryPower() {
        return batteryPower;
    }

    public void setBatteryPower(int batteryPower) {
        this.batteryPower = batteryPower;
    }

    public boolean beep() {
        return beepWithMelody(melody);
    }

    public boolean beepWithMelody(int melody) {
        if (bluetoothLeService == null) {
            bluetoothLeService = new BluetoothLeService();
        }
        if (bluetoothLeService.getConnectionState() == BluetoothLeService.STATE_DISCONNECTED) {
            bluetoothLeService.connect(macAddress);
        }
        if (bluetoothLeService.getConnectionState() != BluetoothLeService.STATE_DISCONNECTED) {
            for (BluetoothGattService gattService : bluetoothLeService.getSupportedGattServices()) {
                List<BluetoothGattCharacteristic> gattCharacteristics =
                        gattService.getCharacteristics();
                for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                    String charUUID = gattCharacteristic.getUuid().toString();
                    if (charUUID.equals(UUID_TO_WRITE)) {
                        characteristic = gattService.getCharacteristic(UUID.fromString(UUID_TO_WRITE));
                    }
                }
            }
            if (characteristic != null) {
                if (melody == 0) {
                    melody = 1;
                }
                characteristic.setValue(Integer.toString(melody));
                bluetoothLeService.setCharacteristicNotification(characteristic, true);
                bluetoothLeService.writeCharacteristic(characteristic);
                Log.i("Sirius", "characteristic written" + melody);
                return true;
            }
        }
        Log.i("Sirius", this.macAddress + "is disconnected");
        return false;
    }

    public BluetoothLeService getBluetoothLeService() {
        return bluetoothLeService;
    }

    public List<Pack> getPacks() {
        return packs;
    }

    public void setPacks(List<Pack> packs) {
        this.packs = packs;
    }

    public void setBluetoothLeService(BluetoothLeService bluetoothLeService) {
        this.bluetoothLeService = bluetoothLeService;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void connect(Context context) throws InterruptedException {
        if (!isConnected) {
            this.context = context;
            this.bluetoothLeService.setContext(context);
            isConnected = true;
            if (this.bluetoothLeService.initialize()) {
                if (getBluetoothLeService().getConnectionState() == BluetoothLeService.STATE_DISCONNECTED) {
                    this.bluetoothLeService.connect(this.getMacAddress());
                }
                TimeUnit.SECONDS.sleep(2);
                if (this.bluetoothLeService.getSupportedGattServices() != null) {
                    for (BluetoothGattService gattService : this.bluetoothLeService.getSupportedGattServices()) {
                        List<BluetoothGattCharacteristic> gattCharacteristics =
                                gattService.getCharacteristics();
                        for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                            String charUUID = gattCharacteristic.getUuid().toString();
                            if (charUUID.equals(UUID_TO_READ)) {
                                characteristic = gattService.getCharacteristic(UUID.fromString(UUID_TO_READ));
                                this.bluetoothLeService.readCharacteristic(characteristic);
                                this.bluetoothLeService.setCharacteristicNotification(characteristic, true);
                                this.bluetoothLeService.setCharactristicNotificationWithDescriptor(
                                        characteristic, CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID);

                                Log.i(context.getString(R.string.app_name), context.getString(R.string.listening_started));
                            }
                        }
                    }
                }
            }
        }
    }

    public String getMelodyFileForPhone() {
        return melodyFileForPhone;
    }

    public void setMelodyFileForPhone(String melodyFileForPhone) {
        this.melodyFileForPhone = melodyFileForPhone;
    }

    public int getMelody() {
        return melody;
    }

    public void setMelody(int melody) {
        this.melody = melody;
    }
}
