package ua.com.mnbs.mitka.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.util.CollectionUtils;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.fragment.PackInfoFragment;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.manager.PictureCreator;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.model.Pack;

public class PackAdapter extends RecyclerView.Adapter<PackAdapter.ViewHolder> {

    public final static int COLUMNS_COUNT = 5;

    private Context context;
    private List<Pack> packs;

    public PackAdapter(Context context, List<Pack> packs) {
        this.context = context;
        this.packs = packs;
    }


    @NonNull
    @Override
    public PackAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_pack, parent, false);
        return new PackAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Pack currentPack = packs.get(position);

        holder.nameTextView.setText(currentPack.getName());
        holder.nameTextView.setOnClickListener( v -> FragmentManager.openFragment(
                new PackInfoFragment(currentPack, position), (BaseActivity) context));
        holder.packIcon.setImageResource(PictureCreator.getPackPicture(currentPack.getMitkas().size()));
    }

    @Override
    public int getItemCount() {
        return packs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;
        Button checkPackButton;
        ImageView packIcon;

        ViewHolder(View view) {
            super(view);
            nameTextView = view.findViewById(R.id.pack_name_text_view);
            checkPackButton = view.findViewById(R.id.pack_check_button);
            packIcon = view.findViewById(R.id.pack_icon);
        }
    }
}
