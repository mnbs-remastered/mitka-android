package ua.com.mnbs.mitka.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Source;

import java.util.ArrayList;
import java.util.List;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.fragment.AccountSettingsFragment;
import ua.com.mnbs.mitka.fragment.HomeListFragment;
import ua.com.mnbs.mitka.fragment.PackFragment;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.receiver.GattUpdateReceiver;
import ua.com.mnbs.mitka.service.BLEScanService;
import ua.com.mnbs.mitka.service.BluetoothLeService;
import ua.com.mnbs.mitka.service.NotificationsService;

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = "BaseActivity";
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    private static final int ACCESS_FINE_LOCATION_PERMISSION = 20;
    private static final int GEO_PERMISSION_GIVEN = 10;
    private static List<Mitka> mitkas;
    private static String email;
    boolean bound = false;
    private Toolbar toolbar;
    private FirebaseFirestore firestore;
    private Bundle bundle;
    private BluetoothLeService bluetoothLeService = new BluetoothLeService();
    private Integer rssi;
    private BroadcastReceiver gattUpdateReceiver = new GattUpdateReceiver();
    private FirebaseDatabase database;
    private FirebaseAuth auth;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                Fragment homeFragment = new HomeListFragment();
                if (mitkas == null) {
                    firestore.collection("users").document(auth.getCurrentUser().getEmail()).collection("mitkas")
                            .get().addOnSuccessListener(queryDocumentSnapshots -> {
                        List<Mitka> mitkas;
                        int mitkasCount = queryDocumentSnapshots.size();
                        if (mitkasCount > 0) {
                            mitkas = queryDocumentSnapshots.toObjects(Mitka.class);
                            bundle = new Bundle();
                            bundle.putParcelableArrayList("mitkas", (ArrayList<? extends Parcelable>) mitkas);
                            connectAndCacheMitkas(mitkas);
                            setMitkas(mitkas);
                        } else {
                            bundle = new Bundle();
                        }
                        initialiseHomeFragment(homeFragment);
                    });
                } else {
                    initialiseHomeFragment(homeFragment);
                }
                return true;
            case R.id.navigation_pack:
                PackFragment packFragment = new PackFragment();
                packFragment.setArguments(bundle);
                FragmentManager.openFragment(packFragment, BaseActivity.this);
                return true;
            case R.id.navigation_account_settings:
                FragmentManager.openFragment(new AccountSettingsFragment(), BaseActivity.this);
                return true;
        }
        return false;
    };

    public static List<Mitka> getMitkas() {
        return mitkas;
    }

    public static void setMitkas(List<Mitka> mitkas) {
        BaseActivity.mitkas = mitkas;
    }

    public static void addMitka(Mitka mitka) {
        if (mitkas == null) {
            mitkas = new ArrayList<>();
        }
        mitkas.add(mitka);
    }

    public static void updateMitkaImage(int mitkaIndex, Mitka mitka) {
        mitkas.set(mitkaIndex, mitka);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser == null) {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
            return;
        }
        bluetoothLeService.setContext(this);
        bluetoothLeService.initialize();
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        Bundle incomingIntentBundle = this.getIntent().getExtras();
        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();

        email = auth.getCurrentUser().getEmail();

        if (incomingIntentBundle != null) {
            int inputCode = incomingIntentBundle.getInt("request code");
            if (inputCode == NotificationsService.NOTIFICATION_KEY) {
                Toast.makeText(this, getString(R.string.map_taking), Toast.LENGTH_LONG).show();
                //TODO: normal implementation
            } else if (inputCode == LoginActivity.NOTIFICATIONS_REQUEST_CODE) {
                String token = getSharedPreferences(getString(R.string.token_label), MODE_PRIVATE).getString(getString(R.string.token_label), "null");
                String email = firebaseUser.getEmail();
                AuthResult info = incomingIntentBundle.getParcelable("info");
                if (info.getAdditionalUserInfo().isNewUser()) {
                    new Handler().postDelayed(() -> updateToken(email, token), 10000);
                } else {
                    updateToken(email, token);
                }

            }
        }

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        registerReceiver(gattUpdateReceiver, GattUpdateReceiver.makeGattUpdateIntentFilter());

        if (!accessFineLocationPermitted()) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
            requestPermissions(permissions, GEO_PERMISSION_GIVEN);
        }


        if (!getSharedPreferences("update", Context.MODE_PRIVATE).contains("update")) {
            database = FirebaseDatabase.getInstance();
            database.goOnline();
            database.getReference("mitkas").keepSynced(true);
            database.getReference("mitkas").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Log.e(TAG, "got mac" + dataSnapshot.getValue());
                    database.goOffline();
                    getSharedPreferences("update", Context.MODE_PRIVATE).edit().putBoolean("update", false).apply();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, databaseError.toString());
                }
            });
        }
//        Intent intent = new Intent(BaseActivity.this, BLEScanService.class);
//        startService(intent);
        navigation.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        startService(new Intent(BaseActivity.this, BLEScanService.class));
        unregisterReceiver(gattUpdateReceiver);
    }

    protected void onPause() {
        super.onPause();
        startService(new Intent(BaseActivity.this, BLEScanService.class));
    }

    public boolean accessFineLocationPermitted() {
        String permission = getString(R.string.permission_fine_location);
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == GEO_PERMISSION_GIVEN) {
            Intent intent = new Intent(BaseActivity.this, BLEScanService.class);
            startService(intent);
        } else if (requestCode == HomeListFragment.PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK) {
            Uri contentDescriber = data.getData();
            String fileName = contentDescriber.getLastPathSegment();
            HomeListFragment.getMitkas().get(0).setMelodyFileForPhone(fileName);
            HomeListFragment.getAdapter().notifyItemChanged(0);
            GattUpdateReceiver.setMelody(contentDescriber);
        }
    }

    private void updateToken(String email, String token) {
        firestore.collection(getString(R.string.users_label)).document(email).update(getString(R.string.token_label), token)
                .addOnSuccessListener(doc -> Log.e(TAG, getString(R.string.token_updated) + token));
    }

    private void initialiseHomeFragment(Fragment homeFragment) {
        homeFragment.setArguments(bundle);
        FragmentManager.openFragment(homeFragment, BaseActivity.this);
        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.getMenu().findItem(R.id.navigation_home).setChecked(true);
    }

    private void connectAndCacheMitkas(List<Mitka> mitkas) {
        int mitkasCount = mitkas.size();
        bundle = new Bundle();
        if (mitkas.size() > 0) {
            for (Mitka mitka : mitkas) {
                try {
                    mitka.connect(this);
                } catch (InterruptedException e) {
                    Log.e(getString(R.string.connection), e.getMessage());
                }
            }
            BaseActivity.setMitkas(mitkas);
            SharedPreferences preferences = this.getSharedPreferences(getString(R.string.mitkas_label), Context.MODE_PRIVATE);
            int cachedCount = preferences.getInt(getString(R.string.counting), 0);
            if (cachedCount != 0 && cachedCount != mitkasCount)
                preferences.edit().putInt(getString(R.string.counting), mitkasCount).apply();
            bundle.putParcelableArrayList(getString(R.string.mitkas_label), (ArrayList<? extends Parcelable>) mitkas);
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public static String getEmail() {
        return email;
    }
}
