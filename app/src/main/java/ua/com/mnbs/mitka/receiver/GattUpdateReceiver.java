package ua.com.mnbs.mitka.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.util.Log;

import androidx.fragment.app.Fragment;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.fragment.HomeMapFragment;
import ua.com.mnbs.mitka.fragment.MitkaInfoFragment;
import ua.com.mnbs.mitka.manager.Beeper;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.service.BluetoothLeService;

public class GattUpdateReceiver extends BroadcastReceiver {

    private static final String TAG = "GattUpdateReceiver";
    private static final String CLICK_MESSAGE = "Click";
    private static final String DOUBLE_CLICK_MESSAGE = "Double click";
    private static Uri melody;


    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
            Log.i(TAG, BluetoothLeService.ACTION_GATT_CONNECTED);
        } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
            Log.i(TAG, BluetoothLeService.ACTION_GATT_DISCONNECTED);
        } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
            Log.i(TAG, BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
            Log.i(TAG, BluetoothLeService.ACTION_DATA_AVAILABLE);
            Log.i(TAG, "data got:" + intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            String message = intent.getStringExtra(BluetoothLeService.EXTRA_DATA);
            if (message.trim().equals(CLICK_MESSAGE)) {
                Beeper.beep(context);
            } else if (message.trim().equals(DOUBLE_CLICK_MESSAGE)) {
                if (this.melody != null) {
                    Beeper.beepOnDoubleClick(context, melody);
                } else {
                    Beeper.beepOnDoubleClickDefault(context);
                }
            }
        } else if (BluetoothLeService.ACTION_RSSI_READ.equals(action)) {
            Integer rssi = intent.getIntExtra(BluetoothLeService.EXTRA_DATA, Integer.MIN_VALUE);
            if (rssi != null) {
                Log.i(TAG, context.getString(R.string.rssi_label) + rssi);
                if (context instanceof BaseActivity) {
                    Fragment fragment = FragmentManager.getCurrentFragment((BaseActivity) context);
                    Log.i(TAG, fragment.toString());
                    if (fragment instanceof MitkaInfoFragment) {
                        ((MitkaInfoFragment) fragment).setRssi(rssi);
                    } else if (fragment instanceof HomeMapFragment) {
                        ((HomeMapFragment) fragment).setRssi(rssi);
                    }
                }
            }

        }
    }

    public static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_RSSI_READ);
        return intentFilter;
    }

    public static Uri getMelody() {
        return melody;
    }

    public static void setMelody(Uri melody) {
        GattUpdateReceiver.melody = melody;
    }
}
