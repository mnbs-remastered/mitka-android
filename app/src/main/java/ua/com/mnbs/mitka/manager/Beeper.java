package ua.com.mnbs.mitka.manager;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

import ua.com.mnbs.mitka.R;

public class Beeper {

    public static void beep(Context context) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.ping);
        mediaPlayer.setLooping(false);
        mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();
    }


    public static void beepOnDoubleClick(Context context, Uri uri) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, uri);
        mediaPlayer.setLooping(false);
        mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();
    }

    public static void beepOnDoubleClickDefault(Context context) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.vovchytsia);
        mediaPlayer.setLooping(false);
        mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();
    }


    public static void sadness(Context context) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.richka);
        mediaPlayer.setLooping(true);
        mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();
    }

}
