package ua.com.mnbs.mitka.service;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.adapter.ExpandedViewListAdapter;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.receiver.GattUpdateReceiver;

public class FloatingViewService extends Service {

    private static final String TAG = "FloatingViewService";
    private static final String TYPE_SINGLE_MITKA = "SingleMitka";
    private static final String TYPE_PACK_MITKA = "PackMitka";

    private final BroadcastReceiver gattUpdateReceiver = new GattUpdateReceiver();

    private WindowManager windowManager;
    private View floatingView;
    private View deleteView;
    private RelativeLayout rootContainer;
    private int counter = 0;
    private int counter_one = 1;
    private boolean longClicked = true;
    private boolean longClickedOne = false;
    private static Mitka mitka;
    private static List<Mitka> mitkasPack;
    private ExpandedViewListAdapter adapter;
    private static View collapsedView;
    private static View expandedView;
    private static ImageView collapsedIv;
    private static String serviceType;

    public FloatingViewService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        floatingView = LayoutInflater.from(this).inflate(R.layout.floating_widget, null);
        deleteView = LayoutInflater.from(this).inflate(R.layout.floating_widget_delete_view, null);
        rootContainer  = floatingView.findViewById(R.id.root_container);
        collapsedView = floatingView.findViewById(R.id.collapse_view);
        expandedView = floatingView.findViewById(R.id.expanded_container);
        collapsedIv = floatingView.findViewById(R.id.collapsed_iv);
        if (serviceType.equals(TYPE_SINGLE_MITKA)) {
            setMitkaImage(mitka.getMitkaImage());
        } else if (serviceType.equals(TYPE_PACK_MITKA)) {
            setMitkaImage(mitkasPack.get(0).getMitkaImage());
        }

        final WindowManager.LayoutParams paramsForWidget;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            paramsForWidget = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

            paramsForWidget.gravity = Gravity.TOP | Gravity.LEFT;
            paramsForWidget.x = 0;
            paramsForWidget.y = 100;
            paramsForWidget.windowAnimations = android.R.style.Animation_Translucent;
        } else {
            paramsForWidget = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

            paramsForWidget.gravity = Gravity.TOP | Gravity.LEFT;
            paramsForWidget.x = 0;
            paramsForWidget.y = 100;
            paramsForWidget.windowAnimations = android.R.style.Animation_Translucent;
        }

        final WindowManager.LayoutParams paramsForDeleteView;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            paramsForDeleteView = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

            paramsForDeleteView.x = 0;
            paramsForDeleteView.y = 2500;
            paramsForDeleteView.width = 1080;
            paramsForWidget.windowAnimations = android.R.style.Animation_Translucent;
        } else {
            paramsForDeleteView = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

            paramsForDeleteView.x = 0;
            paramsForDeleteView.y = 2500;
            paramsForDeleteView.width = 1080;
            paramsForWidget.windowAnimations = android.R.style.Animation_Translucent;
        }

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.addView(floatingView, paramsForWidget);
        windowManager.addView(deleteView, paramsForDeleteView);

        floatingView.findViewById(R.id.root_container).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;
            final Handler handlerd = new Handler();
            final Handler handler = new Handler();

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = paramsForWidget.x;
                        initialY = paramsForWidget.y;

                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        longClicked = true;
                        longClickedOne = true;
                        handlerd.postDelayed((new Runnable() {
                            @Override
                            public void run() {
                                if (++counter < 3) {
                                    handlerd.postDelayed(this, 500);
                                    return;
                                } else {
                                    if (isViewCollapsed() && longClicked) {
                                        collapsedView.setVisibility(View.GONE);
                                        expandedView.setVisibility(View.VISIBLE);
                                    }
                                }
                                handlerd.removeCallbacks(this);
                            }
                        }), 500);
                        handler.postDelayed((new Runnable() {
                            @Override
                            public void run() {
                                if (++counter_one < 2) {
                                    handler.postDelayed(this, 500);
                                    return;
                                } else {
                                    if (isViewCollapsed() && longClickedOne) {
                                        showView(deleteView);
                                    }
                                }
                                handler.removeCallbacks(this);
                            }
                        }), 500);
                        if (serviceType.equals(TYPE_SINGLE_MITKA)) {
                            mitka.beep();
                        } else if (serviceType.equals(TYPE_PACK_MITKA)) {
                            mitkasPack.get(0).beep();
                        }
                        return true;
                    case MotionEvent.ACTION_UP:
                        longClicked = false;
                        hideView(deleteView);
                        if (paramsForWidget.y > 1660) {

                            final Handler handler = new Handler();
                            handler.postDelayed(() -> stopSelf(), 1000);
                            hideView(deleteView);
                            hideView(floatingView);
                        }
                        if ((paramsForWidget.x < 500) && (paramsForWidget.y < 1660)) {
                            animateView(paramsForWidget, 0);
                        } else if ((paramsForWidget.x > 500) && (paramsForWidget.y < 1660)){
                            animateView(paramsForWidget, 900);
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        longClicked = false;
                        paramsForWidget.x = initialX + (int) (event.getRawX() - initialTouchX);
                        paramsForWidget.y = initialY + (int) (event.getRawY() - initialTouchY);

                        windowManager.updateViewLayout(floatingView, paramsForWidget);
                        return true;
                }
                return true;
            }
        });
        this.registerReceiver(gattUpdateReceiver, GattUpdateReceiver.makeGattUpdateIntentFilter());

        RecyclerView recyclerView = expandedView.findViewById(R.id.mitka_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        List<Mitka> mitkas = BaseActivity.getMitkas();
        if (serviceType.equals(TYPE_SINGLE_MITKA)) {
            if (mitkas != null) {
                adapter = new ExpandedViewListAdapter(getApplicationContext(), mitkas.subList(1, mitkas.size()));
                recyclerView.setAdapter(adapter);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.no_mitkas_available), Toast.LENGTH_LONG).show();
            }
        } else if (serviceType.equals(TYPE_PACK_MITKA)) {
            if (mitkasPack != null) {
                adapter = new ExpandedViewListAdapter(getApplicationContext(), mitkasPack);
                recyclerView.setAdapter(adapter);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.no_mitkas_available), Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        unregisterReceiver(gattUpdateReceiver);
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        unregisterReceiver(gattUpdateReceiver);
    }

    private boolean isViewCollapsed() {
        return floatingView == null || floatingView.findViewById(R.id.collapse_view).getVisibility() == View.VISIBLE;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (floatingView != null) windowManager.removeView(floatingView);
    }

    public void showView(View view) {
        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);
        view.animate()
                .alpha(1f)
                .setDuration(500)
                .setListener(null);
    }

    private void hideView(View view) {
        view.setAlpha(1f);
        view.animate()
                .alpha(0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                    }
                });
    }

    public static Mitka getMitka() {
        return mitka;
    }

    public static void setMitka(Mitka mitka) {
        FloatingViewService.mitka = mitka;
        FloatingViewService.serviceType = TYPE_SINGLE_MITKA;
    }

    public static void setListMitka(List<Mitka> mitkasPack) {
        FloatingViewService.mitkasPack = mitkasPack;
        FloatingViewService.serviceType = TYPE_PACK_MITKA;
    }

    public static void collapseView(int mitkaImage) {
        collapsedView.setVisibility(View.VISIBLE);
        expandedView.setVisibility(View.GONE);
        collapsedIv.setImageResource(mitkaImage);
    }

    public static void setMitkaImage(int mitkaImage) {
        collapsedIv.setImageResource(mitkaImage);
    }

    public void animateView(WindowManager.LayoutParams params, int x) {
        ValueAnimator va = ValueAnimator.ofFloat(params.x, x);
        int mDuration = 250;
        va.setDuration(mDuration);
        va.addUpdateListener(animation -> {
            params.x = Math.round((Float) animation.getAnimatedValue());
            windowManager.updateViewLayout(floatingView, params);
        });
        va.start();
    }
}
