package ua.com.mnbs.mitka.model;

public enum RepeatInterval {
    ONCE(0),
    DAILY(1),
    WEEKLY(2);
    int number;

    RepeatInterval(int number) {this.number = number;}

    public String toString() {
        return String.valueOf(number);
    }
}