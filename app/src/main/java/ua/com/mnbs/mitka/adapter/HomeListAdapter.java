package ua.com.mnbs.mitka.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.fragment.HomeListFragment;
import ua.com.mnbs.mitka.fragment.MitkaInfoFragment;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.model.Mitka;

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.ViewHolder> {

    private static final String TAG = "HOME_LIST_ADAPTER";

    private Activity context;
    private List<Mitka> mitkas;
    HomeListAdapter instance;

    public HomeListAdapter(Activity context, List<Mitka> mitkas) {
        this.context = context;
        this.mitkas = mitkas;
    }

    @NonNull
    @Override
    public HomeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (i == 0) {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_phone, viewGroup, false);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_mitka, viewGroup, false);
        }
        view.getLayoutParams().width = viewGroup.getMeasuredWidth() / 2;
        return new HomeListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListAdapter.ViewHolder holder, int position) {

        Mitka currentMitka = mitkas.get(position);

        if (position == 0) {
            holder.chooseMelody.setOnClickListener(View -> {
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFile.setType(context.getString(R.string.type_for_audio));
                context.startActivityForResult(
                        Intent.createChooser(chooseFile, context.getString(R.string.select_melody_label)),
                        HomeListFragment.PICKFILE_RESULT_CODE);
            });
            if (currentMitka.getMelodyFileForPhone() != null) {
                holder.melodyTextView.setText(currentMitka.getMelodyFileForPhone());
            }

        } else {

            holder.myMitkaImageView.setImageResource(currentMitka.getMitkaImage());
            holder.mitkaTitleTextView.setText(currentMitka.getName());
            holder.mitkaSignalTextView.setText(context.getString(R.string.signal_strenght));
            holder.mitkaPowerTextView.setText(Integer.toString(currentMitka.getSignalPower()));
            holder.batteryTextView.setText(context.getString(R.string.power_level));
            holder.progressBar.setVisibility(View.INVISIBLE);
            String name = currentMitka.getName();
            instance = this;

            holder.myMitkaImageView.setOnClickListener(v -> currentMitka.beep());
            holder.findMitkaButton.setOnClickListener(v -> currentMitka.beep());

            holder.mitkaLayout.setOnClickListener(v -> holder.sendData(currentMitka, position));
            CollectionReference mitkasRef = FirebaseFirestore.getInstance().collection("users")
                    .document(FirebaseAuth.getInstance().getCurrentUser().getEmail()).collection("mitkas");
            holder.mitkaLayout.setOnLongClickListener(v -> {
                new AlertDialog.Builder(context)
                        .setTitle("Ви дійсно хочете видалити мітку" + name + "?")
                        .setNegativeButton("Скасувати", ((dialog, which) -> dialog.cancel()))
                        .setPositiveButton("Видалити", ((dialog, which) -> {
                            mitkas.remove(currentMitka);
                            BaseActivity.setMitkas(mitkas);
                            instance.notifyDataSetChanged();
                            Snackbar snackbar = Snackbar.make(holder.mitkaLayout, "Мітку " + name + " видалено", Snackbar.LENGTH_LONG);

                            BaseTransientBottomBar.BaseCallback<Snackbar> callback = new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                @Override
                                public void onDismissed(Snackbar transientBottomBar, int event) {
                                    super.onDismissed(transientBottomBar, event);
                                    mitkasRef.document(currentMitka.getMacAddress()).delete()
                                            .addOnSuccessListener(aVoid -> Log.e(TAG, "success"))
                                            .addOnFailureListener(e -> Log.e(TAG, e.getLocalizedMessage(), e));
                                    try {
                                        currentMitka.getBluetoothLeService().disconnect();
                                    } catch (NullPointerException e) {
                                        Log.e(TAG, "mitka bluetoothLEService is null");
                                    }
                                }
                            };

                            snackbar.setAction("Відновити", v1 -> {
                                snackbar.removeCallback(callback);
                                mitkas.add(currentMitka);
                                BaseActivity.setMitkas(mitkas);
                                notifyDataSetChanged();
                            }).addCallback(callback).show();

                            dialog.dismiss();
                        })).show();
                return false;
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return mitkas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView myMitkaImageView;
        final TextView mitkaTitleTextView;
        final TextView mitkaSignalTextView;
        final TextView mitkaPowerTextView;
        final TextView batteryTextView;
        final ProgressBar progressBar;
        final LinearLayout mitkaLayout;
        final Button findMitkaButton;
        final TextView melodyTextView;
        final Button chooseMelody;

        ViewHolder(View view) {
            super(view);
            myMitkaImageView = view.findViewById(R.id.mitka_info_my_mitka_image);
            mitkaTitleTextView = view.findViewById(R.id.mitka_list_item_title_text_view);
            mitkaSignalTextView = view.findViewById(R.id.mitka_info_signal_text_view);
            mitkaPowerTextView = view.findViewById(R.id.mitka_edit_power_text_view);
            batteryTextView = view.findViewById(R.id.mitka_list_item_battery_text_view);
            progressBar = view.findViewById(R.id.progress_bar);
            mitkaLayout = view.findViewById(R.id.mitka_list_item_one);
            findMitkaButton = view.findViewById(R.id.find_mitka_button);

            melodyTextView = view.findViewById(R.id.melody_text_view);
            chooseMelody = view.findViewById(R.id.select_melody);
        }

        public void sendData(Mitka currentMitka, int position) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("mitka", currentMitka);
            MitkaInfoFragment mitkaInfoFragment = new MitkaInfoFragment(position);
            mitkaInfoFragment.setArguments(bundle);

            FragmentManager.openFragment(mitkaInfoFragment, (BaseActivity) context);
        }
    }
}
