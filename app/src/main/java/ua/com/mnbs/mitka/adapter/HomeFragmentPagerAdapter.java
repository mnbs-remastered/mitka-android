package ua.com.mnbs.mitka.adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.fragment.HomeListFragment;
import ua.com.mnbs.mitka.fragment.HomeMapFragment;

public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private Context context;
    private Bundle bundle;
    private Fragment currentFragment;


    public HomeFragmentPagerAdapter(FragmentManager fm, Context context, Bundle bundle) {
        super(fm);
        this.context = context;
        this.bundle = bundle;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.title_home_list);
            case 1:
                return context.getString(R.string.title_home_map);
        }
        return null;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new HomeListFragment();
                fragment.setArguments(bundle);
                currentFragment = fragment;
                return fragment;
            case 1:
                fragment = new HomeMapFragment();
                fragment.setArguments(bundle);
                currentFragment = fragment;
                return fragment;
        }
        return null;
    }
    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(Fragment currentFragment) {
        this.currentFragment = currentFragment;
    }
}
