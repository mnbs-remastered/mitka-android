package ua.com.mnbs.mitka.model;

import android.os.Parcel;
import android.os.Parcelable;


import java.util.ArrayList;
import java.util.List;

public class Pack implements Parcelable {

    private String name;
    private List<String> mitkas;
    private List<Reminder> reminders;
    private String id;
    private int melody;

    public Pack() {
    }

    public Pack(String name, List<String> mitkas) {
        this(name,mitkas, new ArrayList<>());
    }

    public Pack(String name, List<String> mitkas, List<Reminder> reminders) {
        this.name = name;
        this.mitkas = mitkas;
        this.reminders = reminders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMitkas() {
        return mitkas;
    }

    public void setMitkas(List<String> mitkas) {
        this.mitkas = mitkas;
    }

    public int getMelody() {
        return melody;
    }

    public void setMelody(int melody) {
        this.melody = melody;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeStringList(mitkas);
    }

    protected Pack(Parcel in) {
        name = in.readString();
        mitkas = in.createStringArrayList();
    }

    public static final Creator<Pack> CREATOR = new Creator<Pack>() {
        @Override
        public Pack createFromParcel(Parcel in) {
            return new Pack(in);
        }

        @Override
        public Pack[] newArray(int size) {
            return new Pack[size];
        }
    };

    public List<Reminder> getReminders() {
        return reminders;
    }

    public void setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
