package ua.com.mnbs.mitka.manager;

import androidx.fragment.app.Fragment;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;

public class FragmentManager {

    public static void openFragment(Fragment fragment, BaseActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    public static void openFragmentWithBackStack(Fragment fragment, BaseActivity activity) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public static Fragment getCurrentFragment(BaseActivity activity) {
        return activity.getSupportFragmentManager().findFragmentById(R.id.container);
    }
}
