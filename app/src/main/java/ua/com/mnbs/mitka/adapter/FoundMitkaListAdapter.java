package ua.com.mnbs.mitka.adapter;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.fragment.EditMitkaFragment;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.service.BluetoothLeService;

public class FoundMitkaListAdapter extends RecyclerView.Adapter<FoundMitkaListAdapter.ViewHolder> {


    private Activity context;
    private List<Mitka> mitkas;
    private BluetoothAdapter bluetoothAdapter;

    public FoundMitkaListAdapter(Activity context, List<Mitka> mitkas, BluetoothAdapter bluetoothAdapter) {
        this.context = context;
        this.mitkas = mitkas;
        this.bluetoothAdapter = bluetoothAdapter;
    }

    @NonNull
    @Override
    public FoundMitkaListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_item_found_mitka, viewGroup, false);
        return new FoundMitkaListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FoundMitkaListAdapter.ViewHolder holder, int position) {
        Mitka currentMitka = mitkas.get(position);
        holder.mitkaMacAddress.setText(currentMitka.getMacAddress());
        holder.position = position;
    }

    private void addMitka(Mitka mitka) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(context.getString(R.string.mitka_label), mitka);
        bundle.putBoolean("justFound", true);
        Fragment fragment = new EditMitkaFragment();
        fragment.setArguments(bundle);
        FragmentManager.openFragment(fragment, (BaseActivity) context);
    }

    @Override
    public int getItemCount() {
        return mitkas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mitkaMacAddress;
        final TextView mitkaSignalPowerTextView;
        int position;

        ViewHolder(View view) {
            super(view);
            mitkaMacAddress = view.findViewById(R.id.mitka_list_item_mac_address_text);
            mitkaSignalPowerTextView = view.findViewById(R.id.mitka_edit_power_text_view);
            view.setOnClickListener(v -> {
                bluetoothAdapter.stopLeScan(null);
                Mitka mitka = mitkas.get(position);
                mitka.setOwnerId(FirebaseAuth.getInstance().getUid());

                BluetoothLeService service = new BluetoothLeService();
                service.connect(mitka.getMacAddress());

                addMitka(mitka);
            });
        }
    }
}
