package ua.com.mnbs.mitka.fragment;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.adapter.FoundMitkaListAdapter;
import ua.com.mnbs.mitka.model.Mitka;

public class FoundMitkasFragment extends Fragment {

    private static final int ACCESS_FINE_LOCATION_PERMISSION = 20;
    private static final int SCAN_PERIOD = 10000;

    private Handler handler;
    private boolean scanning;
    private BluetoothAdapter bluetoothAdapter;
    private FoundMitkaListAdapter adapter;
    private List<Mitka> mitkas;
    private Set<String> deviceAddresses;
    public static final String TAG = "FOUND_MITKAS_FRAGMENT";
    private ImageView searchMitkaImage;
    private int counter = 0;
    private FirebaseDatabase database;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_found_mitka, container, false);
        //TODO
        TextView emptyTextView = rootView.findViewById(R.id.empty_view);
        LinearLayout findLayout = rootView.findViewById(R.id.finding_layout);
        recyclerView = rootView.findViewById(R.id.found_mitka_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        database = FirebaseDatabase.getInstance();
        searchMitkaImage = findLayout.findViewById(R.id.search_mitka_image);
        return findMitka(rootView, recyclerView);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            handler.postDelayed(() -> {
                scanning = false;
                bluetoothAdapter.stopLeScan(leScanCallback);
            }, SCAN_PERIOD);

            scanning = true;
            bluetoothAdapter.startLeScan(leScanCallback);
        } else {
            scanning = false;
            bluetoothAdapter.stopLeScan(leScanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback leScanCallback =
            (device, rssi, scanRecord) -> {
                if (!deviceAddresses.contains(device.getAddress())) {
                    database.getReference("mitkas").child(device.getAddress())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                deviceAddresses.add(device.getAddress());
                                Log.i(TAG, device.getAddress());
                                deviceAddresses.add(device.getAddress());
                                mitkas.add(new Mitka(device.getAddress(), 50));
                                adapter.notifyItemInserted(mitkas.size() - 1);
                                recyclerView.setBackgroundColor(getResources().getColor(R.color.grayBackground));
                                if (getView() != null) {
                                    LinearLayout findLayout = getView().findViewById(R.id.finding_layout);
                                    findLayout.setVisibility(View.GONE);

                                    if (mitkas.isEmpty()) {
                                        TextView emptyTextView = getView().findViewById(R.id.empty_view);
                                        emptyTextView.setVisibility(View.VISIBLE);
                                    }
                                }
                            } else {
                                Log.i(TAG, "value doesn't exist");
                                deviceAddresses.add(device.getAddress());
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.e(TAG, databaseError.toString());
                            deviceAddresses.add(device.getAddress());
                        }
                    });
                }
            };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SwipeRefreshLayout swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(() -> {
            RecyclerView recyclerView = view.findViewById(R.id.found_mitka_recyclerview);
            findMitka(view, recyclerView);
            swipeContainer.setRefreshing(false);
        });
        Handler handlerForImage = new Handler();
        handlerForImage.postDelayed((new Runnable() {
            @Override
            public void run() {
                if (++counter < 2) {
                    handlerForImage.postDelayed(this, 500);
                    searchMitkaImage.setImageResource(R.drawable.search0);
                    return;
                } else if (counter < 3) {
                    handlerForImage.postDelayed(this, 500);
                    searchMitkaImage.setImageResource(R.drawable.search1);
                    return;
                } else if (counter < 4) {
                    handlerForImage.postDelayed(this, 500);
                    searchMitkaImage.setImageResource(R.drawable.search2);
                    return;
                } else if (counter < 5) {
                    handlerForImage.postDelayed(this, 500);
                    searchMitkaImage.setImageResource(R.drawable.logo_no_gradient);
                    counter = 0;
                    return;
                }
                handlerForImage.removeCallbacks(this);
            }
        }), 400);
        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
    }

    public boolean accessFineLocationPermitted() {
        String permission = getString(R.string.permission_fine_location);
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private View findMitka(View rootView, RecyclerView recyclerView) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        TextView emptyTextView = rootView.findViewById(R.id.empty_view);
        LinearLayout findLayout = rootView.findViewById(R.id.finding_layout);
        if (bluetoothAdapter == null) {
            Toast.makeText(getActivity(), getString(R.string.bluetooth_unable), Toast.LENGTH_LONG).show();
        } else if (!bluetoothAdapter.isEnabled()) {
            findLayout.setVisibility(View.GONE);
            emptyTextView.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), getString(R.string.turn_bluetooth_on), Toast.LENGTH_LONG).show();
        } else {
            findLayout.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.GONE);

        }
        handler = new Handler();
        deviceAddresses = new HashSet<>();

        if (!accessFineLocationPermitted()) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_PERMISSION);
        }

        if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(getContext(), getString(R.string.BLE_not_support), Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }

        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (this.bluetoothAdapter == null) {
            Toast.makeText(getContext(), getString(R.string.bluetooth_unable), Toast.LENGTH_SHORT).show();
            return rootView;
        }

        mitkas = new ArrayList<>();
        adapter = new FoundMitkaListAdapter(getActivity(), mitkas, this.bluetoothAdapter);

        recyclerView.setAdapter(adapter);


        scanLeDevice(true);
        return rootView;
    }
}
