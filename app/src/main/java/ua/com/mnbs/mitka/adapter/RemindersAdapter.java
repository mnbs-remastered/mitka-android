package ua.com.mnbs.mitka.adapter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Collections2;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.j2objc.annotations.ObjectiveCName;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.model.Pack;
import ua.com.mnbs.mitka.model.Reminder;
import ua.com.mnbs.mitka.model.RepeatInterval;
import ua.com.mnbs.mitka.receiver.AlarmReceiver;

import static android.content.Context.ALARM_SERVICE;

public class RemindersAdapter extends RecyclerView.Adapter<RemindersAdapter.ViewHolder> {

    public static final long MILLIS_IN_DAY = 86400000;
    public static final long MILLIS_IN_WEEK = 604800000;
    private static final String TAG = "REMINDERS_ADAPTER";
    private Context context;
    private List<Reminder> reminders;
    private int currentDay;
    private int hour;
    private int minut;
    private Pack pack;
    private Mitka mitka;
    private AlarmManager manager;
    private int index;


    public RemindersAdapter(Context context, List<Reminder> reminders, Pack pack, int index) {
        this.context = context;
        this.reminders = reminders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            currentDay = LocalDate.now().getDayOfWeek().getValue();
        }
        this.pack = pack;
        manager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        this.index = index;
    }

    public RemindersAdapter(Context context, List<Reminder> reminders, Mitka mitka, int index) {
        this.context = context;
        this.reminders = reminders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            currentDay = LocalDate.now().getDayOfWeek().getValue();
        }
        this.mitka = mitka;
        manager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        this.index = index;
    }

    @NonNull
    @Override
    public RemindersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_reminder, parent, false);
        return new RemindersAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RemindersAdapter.ViewHolder holder, int position) {
        Reminder currentReminder = reminders.get(position);
        AtomicBoolean boot = new AtomicBoolean(true);

        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        int currentHour = currentTime.getHours();
        int currentMinute = currentTime.getMinutes();
        int currentDay = currentTime.getDay();

        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(context, R.array.repetitions, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.repeatSpinner.setAdapter(adapter);

        holder.reminderSwitch.setChecked(currentReminder.isActive());
        holder.repeatSpinner.setSelection(Integer.parseInt(currentReminder.getRepeatInterval().toString()));
        if (currentReminder.getTime() != null) {
            int reminderHour = currentReminder.getTime().toDate().getHours();
            int reminderMinutes = currentReminder.getTime().toDate().getMinutes();
            holder.chooseTimeEditText.setText(String.format("%d:%s",reminderHour,
                    reminderMinutes<10? "0" + reminderMinutes : "" + reminderMinutes));
        }
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {
            holder.chooseTimeEditText.setText(String.format("%d:%s", hourOfDay, minute < 10 ? "0" + minute : "" + minute));
            hour = hourOfDay;
            minut = minute;
            setUpAlarm(currentReminder, currentDay);
        }, currentHour, currentMinute, true);
        holder.chooseTimeEditText.setOnTouchListener((v, event) -> {
            if (!timePickerDialog.isShowing()) {
                timePickerDialog.show();
            }
            return false;
        });

        holder.repeatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!boot.get()) {
                    switch (position) {
                        case 0:
                            cancelAlarm(currentReminder);
                            currentReminder.setRepeatInterval(RepeatInterval.ONCE);
                            holder.daysSpinner.setVisibility(View.GONE);
                            holder.daysSpinner.setSelection(0);
                            setUpAlarm(currentReminder, currentDay);
                            break;
                        case 1:
                            cancelAlarm(currentReminder);
                            currentReminder.setRepeatInterval(RepeatInterval.DAILY);
                            holder.daysSpinner.setVisibility(View.GONE);
                            holder.daysSpinner.setSelection(0);
                            setUpAlarm(currentReminder, currentDay);
                            break;
                        case 2:
                            cancelAlarm(currentReminder);
                            currentReminder.setRepeatInterval(RepeatInterval.WEEKLY);
                            holder.daysSpinner.setVisibility(View.VISIBLE);
                            holder.daysSpinner.setSelection(currentDay - 1);

                            holder.daysSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    setUpAlarm(currentReminder, position);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                    }
                } else {
                    boot.set(false);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                holder.daysSpinner.setVisibility(View.GONE);
            }
        });

        holder.reminderSwitch.setOnCheckedChangeListener( (buttonView, isChecked) -> {
                if (isChecked) {
                    currentReminder.setActive(true);
                    setUpAlarm(currentReminder, currentDay);
                } else {
                    currentReminder.setActive(false);
                    cancelAlarm(currentReminder);
                }
        });

        holder.deleteReminderButton.setOnClickListener(v -> {
            reminders.remove(position);
            cancelAlarm(currentReminder);
            notifyDataSetChanged();
            String collectionName;
            String unitId;
            if (pack != null) {
                collectionName = "packs";
                unitId = pack.getName();
            } else {
                collectionName = "mitkas";
                unitId = mitka.getMacAddress();
            }
            if (currentReminder.getId() != null) {
                FirebaseFirestore.getInstance()
                        .collection("users")
                        .document(BaseActivity.getEmail())
                        .collection(collectionName)
                        .document(unitId)
                        .collection("reminders")
                        .document(currentReminder.getId())
                        .delete()
                        .addOnFailureListener(Throwable::printStackTrace);
            }
        });
    }

    private void cancelAlarm(Reminder currentReminder) {
        modifyAlarm(currentReminder,0, false);
    }

    private void setUpAlarm(Reminder currentReminder, int dayOfTheWeek) {
        modifyAlarm(currentReminder,dayOfTheWeek + 1, true);
    }

    private void modifyAlarm(Reminder currentReminder, int dayOfTheWeek, boolean setUp) {
        Calendar calendar = Calendar.getInstance();
        boolean weekly = currentReminder.getRepeatInterval() == RepeatInterval.WEEKLY;
        if (weekly) {
            calendar.set(Calendar.DAY_OF_WEEK, dayOfTheWeek + 1);
        }
        calendar.set(Calendar.AM_PM, hour < 12 ? Calendar.AM: Calendar.PM);
        calendar.set(Calendar.DAY_OF_WEEK, dayOfTheWeek);
        calendar.set(Calendar.HOUR, hour % 12);
        calendar.set(Calendar.MINUTE, minut);
        calendar.set(Calendar.SECOND, 0);

        if ((weekly && dayOfTheWeek + 1 == new Timestamp(System.currentTimeMillis()).getDay()) | !weekly) {

            Intent intent = new Intent(context, AlarmReceiver.class);
            //intent.putExtra("reminder", currentReminder);
            int requestCode;
            if (mitka == null) {
//                List<Mitka> mitkas = new ArrayList<>(Collections2.filter(BaseActivity.getMitkas(),
//                        mitka -> pack.getMitkas().contains(mitka.getMacAddress())));
//                intent.putParcelableArrayListExtra("mitkas", (ArrayList<? extends Parcelable>) mitkas);
                requestCode = index * 10;
                //intent.putExtra("mitkas", marshal(mitkas));
            } else {
               // intent.putExtra("mitka", (Parcelable) mitka);
                requestCode = index;
            }
            intent.setAction("" + requestCode);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0 , intent, 0);
            com.google.firebase.Timestamp timestamp = new com.google.firebase.Timestamp(calendar.getTime());
            currentReminder.setTime(timestamp);
            if (setUp) {
                currentReminder.setTime(new com.google.firebase.Timestamp(calendar.getTime()));
                if (currentReminder.getRepeatInterval() == RepeatInterval.ONCE) {
                    manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                } else {
                    manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            (currentReminder.getRepeatInterval() == RepeatInterval.DAILY) ? MILLIS_IN_DAY : MILLIS_IN_WEEK, pendingIntent);
                }
            } else {
                manager.cancel(pendingIntent);
            }
        }
    }
    @Override
    public int getItemCount() {
        return reminders.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private Switch reminderSwitch;
        private Spinner repeatSpinner;
        private EditText chooseTimeEditText;
        private Spinner daysSpinner;
        ImageButton deleteReminderButton;

        public ViewHolder(@NonNull View view) {
            super(view);
            reminderSwitch = view.findViewById(R.id.switch_reminder);
            repeatSpinner = view.findViewById(R.id.spinner_repetition);
            chooseTimeEditText = view.findViewById(R.id.time_picker);
            daysSpinner = view.findViewById(R.id.spinner_days);
            deleteReminderButton = view.findViewById(R.id.button_delete_reminder);
        }
    }
}
