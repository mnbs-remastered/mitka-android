package ua.com.mnbs.mitka.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.model.Mitka;

public class NewMitkaInPackAdapter extends RecyclerView.Adapter<NewMitkaInPackAdapter.ViewHolder> {

    private Context context;
    private List<Mitka> mitkas;
    private List<Mitka> addedMitkas;
    private MitkasPackAdapter packAdapter;

    public NewMitkaInPackAdapter(Context context, List<Mitka> mitkas) {
        this.context = context;
        this.mitkas = mitkas;
    }

    public NewMitkaInPackAdapter(Context context, List<Mitka> allMitkas, List<Mitka> addedMitkas, MitkasPackAdapter packAdapter) {
        this.context = context;
        this.mitkas = allMitkas;
        this.addedMitkas = addedMitkas;
        this.packAdapter = packAdapter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_new_mitka_in_pack, parent, false);
        return new NewMitkaInPackAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mitka currentMitka = mitkas.get(position);

        holder.mitkaName.setText(currentMitka.getName());
        holder.mitkaImage.setImageResource(currentMitka.getMitkaImage());

        holder.itemView.setOnClickListener(v -> {
            if (addedMitkas.contains(mitkas.get(position))) {
                addedMitkas.remove(mitkas.get(position));
                holder.itemView.setBackgroundColor(Color.WHITE);
            } else {
                addedMitkas.add(mitkas.get(position));
                holder.itemView.setBackgroundColor(Color.parseColor("#405765C2"));
            }
            notifyDataSetChanged();
            packAdapter.notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return mitkas.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mitkaImage;
        private TextView mitkaName;
        private View itemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mitkaImage = itemView.findViewById(R.id.mitka_image);
            mitkaName = itemView.findViewById(R.id.mitka_name);
            this.itemView = itemView;
        }
    }
}
