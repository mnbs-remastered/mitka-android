package ua.com.mnbs.mitka.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.adapter.PackAdapter;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.model.Pack;


public class PackFragment extends Fragment {

    private PackAdapter packAdapter;
    private List<Pack> packs;

    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pack, null);

        recyclerView = rootView.findViewById(R.id.pack_list);

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        FirebaseFirestore.getInstance()
                .collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getEmail())
                .collection("packs")
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    packs = documentSnapshot.isEmpty() ? new ArrayList<>() : documentSnapshot.toObjects(Pack.class);
                    packAdapter = new PackAdapter(getActivity(), packs);
                    recyclerView.setAdapter(packAdapter);
                });


        rootView.findViewById(R.id.add_pack_fab).setOnClickListener(v -> {
            EditPackFragment editPackFragment = new EditPackFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean("justCreated", true);
            editPackFragment.setArguments(bundle);
            FragmentManager.openFragment(editPackFragment, (BaseActivity) getActivity());
        });
        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(null);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}