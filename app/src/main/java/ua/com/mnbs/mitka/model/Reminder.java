package ua.com.mnbs.mitka.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.firebase.Timestamp;

public class Reminder implements Parcelable {
    private String parent;
    private RepeatInterval repeatInterval;
    private boolean active;
    private Timestamp time;
    private String id;

    public Reminder () {
    }

    public Reminder(String parent, RepeatInterval repeatInterval, boolean active) {
        this.parent = parent;
        this.repeatInterval = repeatInterval;
        this.active = active;
    }

    protected Reminder(Parcel in) {
        parent = in.readString();
        active = in.readByte() != 0;
        id = in.readString();
        time = in.readParcelable(Timestamp.class.getClassLoader());
    }

    public Reminder (String parent) {
        this.parent = parent;
    }

    public boolean isComplete() {
        boolean returnValue = (parent != null) && repeatInterval != null && time !=null;
        return returnValue;
    }

    public static final Creator<Reminder> CREATOR = new Creator<Reminder>() {
        @Override
        public Reminder createFromParcel(Parcel in) {
            return new Reminder(in);
        }

        @Override
        public Reminder[] newArray(int size) {
            return new Reminder[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(parent);
        dest.writeByte((byte) (active ? 1 : 0));
        dest.writeString(id);
        dest.writeParcelable(time, flags);
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public RepeatInterval getRepeatInterval() {
        return repeatInterval;
    }

    public void setRepeatInterval(RepeatInterval repeatInterval) {
        this.repeatInterval = repeatInterval;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof Reminder) {
            Reminder second = (Reminder) obj;
            try {
                if (id.equals(second.getId())) {
                    boolean returnValue = time.equals(second.getTime()) && repeatInterval.equals(second.getRepeatInterval())
                            && active == second.isActive();
                    return returnValue;
                } else {
                    return false;
                }
            } catch (NullPointerException e) {
                boolean returnValue = id !=null && second.getId() !=null;
                return returnValue;
            }
        } else {
            return false;
        }
    }
}
