package ua.com.mnbs.mitka.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.autofill.AutofillManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;

import ua.com.mnbs.mitka.R;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LOGIN_ACTIVITY";
    private static final int RC_SIGN_IN = 4832;
    public static final int NOTIFICATIONS_REQUEST_CODE = 228;

    FirebaseAuth auth;
    Button logInButton;
    Button registerButton;
    EditText emailView;
    EditText passwordView;
    AutofillManager afm;
    ImageButton resetPasswordButton;
    FirebaseFirestore firestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        auth = FirebaseAuth.getInstance();

        logInButton = findViewById(R.id.button_log_in);
        logInButton.setOnClickListener(event -> logInEmailPassword());

        registerButton = findViewById(R.id.button_register_intent);
        registerButton.setOnClickListener(event ->
                startActivity(new Intent(this, RegistrationActivity.class))
        );

        firestore = FirebaseFirestore.getInstance();

        emailView = findViewById(R.id.login_email);
        passwordView = findViewById(R.id.login_password);
        resetPasswordButton = findViewById(R.id.button_reset_password);

        resetPasswordButton.setOnClickListener(v -> {
            View resetPasswordView = View.inflate(this,R.layout.dialog_change_password_use_email, null);
            EditText resetPasswordEmail = resetPasswordView.findViewById(R.id.edit_text_password_reset);
            new AlertDialog.Builder(this).setTitle("Відновити пароль")
                    .setView(resetPasswordView)
                    .setPositiveButton("Підтвердити", ((dialog, which) ->
                            auth.sendPasswordResetEmail(resetPasswordEmail.getText().toString()).addOnSuccessListener(aVoid -> {
                                dialog.dismiss();
                                Toast.makeText(this, "Reset email was sent to your email address",Toast.LENGTH_LONG).show();
                            }))
                    ).setNegativeButton("Скасувати", (dialog, which) -> dialog.cancel())
                    .create()
                    .show();
        });

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            afm = this.getSystemService(AutofillManager.class);
            emailView.setAutofillHints(View.AUTOFILL_HINT_EMAIL_ADDRESS);
            passwordView.setAutofillHints(View.AUTOFILL_HINT_PASSWORD);
            if (afm != null) {
                afm.requestAutofill(emailView);
                afm.requestAutofill(passwordView);
                afm.commit();
            }
        }

        final Button googleSignInButton = findViewById(R.id.button_google_sign_in);
        googleSignInButton.setOnClickListener(event -> {
            LinearLayout loginLinearLayout = findViewById(R.id.login_linear_layout);
            loginLinearLayout.setVisibility(View.GONE);
            ProgressBar progressBar = findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestProfile()
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();
            GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, gso);
            Intent signInIntent = googleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignIn.getSignedInAccountFromIntent(data).addOnSuccessListener(googleSignInAccount -> {
                AuthCredential credential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
                auth.signInWithCredential(credential)
                        .addOnSuccessListener(this, this::updateUi)
                        .addOnFailureListener(e -> Log.e(TAG, getString(R.string.google_sign_in_failure), e));
            }).addOnFailureListener(e -> {
                Log.e(TAG, getString(R.string.error_label) + e.getLocalizedMessage(), e);
                LinearLayout loginLinearLayout = findViewById(R.id.login_linear_layout);
                ProgressBar progressBar = findViewById(R.id.progressBar);
                loginLinearLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            });
        }
    }

    private void logInEmailPassword() {
        String email = emailView.getText().toString();
        String password = passwordView.getText().toString();
        LinearLayout loginLinearLayout = findViewById(R.id.login_linear_layout);
        loginLinearLayout.setVisibility(View.GONE);
        ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        if (email.length() > 3 && password.length() > 6) {
            auth.signInWithEmailAndPassword(email, password).addOnSuccessListener(this::updateUi).addOnFailureListener(e -> {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                loginLinearLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            });
        } else {
            Toast.makeText(this, getString(R.string.email_pass_warning), Toast.LENGTH_LONG).show();
            loginLinearLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void updateUi(AuthResult info) {
        Log.d(TAG, getString(R.string.credential_sign_in));
        Intent loginIntent = new Intent(this, BaseActivity.class);
        loginIntent.putExtra(getString(R.string.request_code_label), NOTIFICATIONS_REQUEST_CODE);
        loginIntent.putExtra("info", info);
        firestore.collection("users").document(info.getUser().getEmail()).collection("mitkas").get()
                .addOnSuccessListener(queryDocumentSnapshots -> startActivity(loginIntent));
    }
}
