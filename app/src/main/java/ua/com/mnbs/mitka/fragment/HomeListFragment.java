package ua.com.mnbs.mitka.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.adapter.HomeListAdapter;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.receiver.GattUpdateReceiver;

public class HomeListFragment extends Fragment {

    public static final int PICKFILE_RESULT_CODE = 8778;

    private static HomeListAdapter adapter;
    private static List<Mitka> mitkas;
    private Button chooseMelody;
    private TextView melodyTextView;
    private FloatingActionButton floatingActionButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_list, container, false);

        RecyclerView recyclerView = rootView.findViewById(R.id.mitka_list);
        floatingActionButton = rootView.findViewById(R.id.add_mitka_fab);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        try {
            mitkas = BaseActivity.getMitkas();
            if (mitkas != null) {
                if (!mitkas.get(0).getName().equals("Phone")) {
                    mitkas.add(0, new Mitka(R.drawable.icon_smartphone, "Phone", 0, 0));
                }
                adapter = new HomeListAdapter(getActivity(), mitkas);
                recyclerView.setAdapter(adapter);
            } else {
                mitkas = new ArrayList<>();
                mitkas.add(0, new Mitka(R.drawable.icon_smartphone, "Phone", 0, 0));
                adapter = new HomeListAdapter(getActivity(), mitkas);
                recyclerView.setAdapter(adapter);
            }
        } catch (NullPointerException e) {
            TextView emptyTextView = rootView.findViewById(R.id.empty_view);
            emptyTextView.setVisibility(View.VISIBLE);
        }
        floatingActionButton.setOnClickListener(v -> FragmentManager.openFragment(new FoundMitkasFragment(), (BaseActivity) getActivity()));
        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(null);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public static HomeListAdapter getAdapter() {
        return adapter;
    }

    public static void setAdapter(HomeListAdapter adapter) {
        HomeListFragment.adapter = adapter;
    }

    public static List<Mitka> getMitkas() {
        return mitkas;
    }

    public static void setMitkas(List<Mitka> mitkas) {
        HomeListFragment.mitkas = mitkas;
    }
}
