package ua.com.mnbs.mitka.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Map;

public class User implements Parcelable {

    private List<Pack> packs;
    private String email;
    private String id;
    private Map<String,String> settings;


    public User() {

    }

    public User(List<Pack> packs, String email, String id, Map<String, String> settings) {
        this.packs = packs;
        this.email = email;
        this.id = id;
        this.settings = settings;
    }

    protected User(Parcel in) {
        in.readTypedList(packs, Pack.CREATOR);
        email = in.readString();
        id = in.readString();
        in.readMap(settings, String.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(packs);
        dest.writeString(email);
        dest.writeString(id);
        dest.writeMap(settings);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public List<Pack> getPacks() {
        return packs;
    }

    public void setPacks(List<Pack> packs) {
        this.packs = packs;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, String> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, String> settings) {
        this.settings = settings;
    }
}
