package ua.com.mnbs.mitka.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.Timestamp;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.model.Mitka;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            FirebaseDatabase database;
            database = FirebaseDatabase.getInstance();
            database.setPersistenceCacheSizeBytes(100000000L);
            database.setPersistenceEnabled(true);
            database.goOffline();
        } catch (DatabaseException e) {
            e.printStackTrace();
        } finally {
            Intent intent = new Intent(this, BaseActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
