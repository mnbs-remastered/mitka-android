package ua.com.mnbs.mitka.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.model.Mitka;

public class BLEScanService extends Service {

    private static final long SCAN_PERIOD = 10000;
    private static final String TAG = BLEScanService.class.getSimpleName();

    private static final String CHANNEL_ID = "everlasting";

    private static final long UPD_INTERVAL = 5000;
    private static final long UPD_INTERVAL_FASTEST = UPD_INTERVAL/2;
    private static final int NOTIFICATIONS_ID = 1488;
    private NotificationManager notificationManager;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location location;
    private String desiredAddress;

    private Context context;
    private final IBinder binder = new LocalBinder();
    private BluetoothAdapter bluetoothAdapter;
    private Handler handler;
    private boolean scanning;

    private List<Mitka> mitkas;
    private Set<String> foundAddresses = new HashSet<>();
    FirebaseDatabase database;

    public BLEScanService() {
    }

    public BLEScanService(Context context) {
        this.context = context;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class LocalBinder extends Binder {
        BLEScanService getService() {
            return BLEScanService.this;
        }
    }

    @Override
    public void onCreate() {
        database = FirebaseDatabase.getInstance();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };
        createLocationRequest();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Sirius",NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Handler().postDelayed(() -> {
            handler = new Handler();
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter != null) {
                if (bluetoothAdapter.isEnabled()) {
                    scanLeDevice(true);
                }
            }
        }, SCAN_PERIOD);
        return super.onStartCommand(intent, flags, startId);
    }

    public void scanLeDevice(boolean enable) {
        foundAddresses.clear();
        if (enable) {
            handler.postDelayed(() -> {
                scanning = false;
                bluetoothAdapter.stopLeScan(scanCallback);
                handler.postDelayed(() -> scanLeDevice(true), SCAN_PERIOD);
            }, SCAN_PERIOD);
            scanning = true;
            bluetoothAdapter.startLeScan(scanCallback);
        } else {
            scanning = false;
            bluetoothAdapter.stopLeScan(scanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback scanCallback = (device, rssi, scanRecord) -> {
        String macAddress = device.getAddress();
        if (!foundAddresses.contains(macAddress)) {
            isMitka(device);
        }
    };

    private void isMitka(BluetoothDevice device) {
        try {
            String name = device.getName();
            database.getReference("mitkas").child(" " + device.getAddress()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        foundAddresses.add(device.getAddress());
                        Log.i(TAG, device.getAddress());
                        desiredAddress = device.getAddress();
                        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                    }
                    else {
                        Log.e(TAG, "value doesn't exist");
                        foundAddresses.add(device.getAddress());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, databaseError.toString());
                    foundAddresses.add(device.getAddress());
                }
            });
        } catch (NullPointerException e) {
            foundAddresses.add(device.getAddress());
        }
    }

    private void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPD_INTERVAL);
        locationRequest.setFastestInterval(UPD_INTERVAL_FASTEST);
        locationRequest.setSmallestDisplacement(10f);
    }

    private void onNewLocation(Location lastLocation) {
        if(location != lastLocation) {
            location = lastLocation;
            if (serviceInForeground(this)) {
                notificationManager.notify(NOTIFICATIONS_ID, getNotification());
            } else {
                startForeground(NOTIFICATIONS_ID, getNotification());
                notificationManager.notify(NOTIFICATIONS_ID, getNotification());
            }
            database.getReference("mitkas").child(desiredAddress).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    FirebaseFirestore.getInstance().collection("users")
                            .document((String) dataSnapshot.getValue()).collection("mitkas")
                            .document(desiredAddress)
                            .update("lastSeen", System.currentTimeMillis(), "location",
                                    new GeoPoint(lastLocation.getLatitude(), lastLocation.getLongitude()))
                            .addOnSuccessListener(aVoid -> Log.d(TAG, "sucessfully updated location"))
                            .addOnFailureListener(e -> Log.e(TAG,"update failed", e));
                    fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                    stopForeground(true);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, databaseError.toString());
                }
            });

        }
    }

    private Notification getNotification() {
        String text = location.getLatitude() + "/" + location.getLongitude();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Всевидяче око дивиться за тобою")
                .setContentText(text)
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_MIN)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(text)
                .setWhen(System.currentTimeMillis());
        builder.setChannelId(CHANNEL_ID);
        Log.i(TAG, location.getLatitude() + "/" + location.getLongitude());
        return builder.build();
    }

    private boolean serviceInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (getClass().getName().equals(serviceInfo.service.getClassName())) {
                if (serviceInfo.foreground) {
                    return true;
                }
            }
        }
        return false;
    }
}
