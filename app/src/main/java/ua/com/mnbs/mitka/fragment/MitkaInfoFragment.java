package ua.com.mnbs.mitka.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.common.collect.Collections2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.adapter.RemindersAdapter;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.model.ConnectionStatus;
import ua.com.mnbs.mitka.model.Melody;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.model.Reminder;
import ua.com.mnbs.mitka.model.RepeatInterval;
import ua.com.mnbs.mitka.service.BluetoothLeService;
import ua.com.mnbs.mitka.service.FloatingViewService;

public class MitkaInfoFragment extends Fragment {

    private static final String TAG = "MITKA_INFO";
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    private Mitka currentMitka;
    private int DISCONNECTED = 0;
    private Integer currentRssi;
    private TextView powerOfSignal;
    private TextView mitkaMelody;
    private ImageButton kebab;
    private Button radarButton;
    private CollectionReference reminderReference;
    private List<Reminder> reminders;
    private List<Reminder> oldReminders;
    private RecyclerView remindersRecyclerView;
    private Button addReminder;
    private int index;

    public MitkaInfoFragment(int index) {
        this.index = index;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        assert getArguments() != null;
        currentMitka = getArguments().getParcelable(getString(R.string.mitka_label));
        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            BottomNavigationView mBottomNavigationView = getActivity().findViewById(R.id.navigation);
            mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
            toolbar.setNavigationIcon(null);
        });
        return inflater.inflate(R.layout.fragment_mitka_info, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView mitkaImageView = view.findViewById(R.id.mitka_info_my_mitka_image);
        powerOfSignal = view.findViewById(R.id.mitka_edit_power_text_view);
        TextView macAddressView = view.findViewById(R.id.mitka_info_mac_address);
        TextView statusTextView = view.findViewById(R.id.mitka_info_status_text_view);
//        TextView lastSeenTextView = view.findViewById(R.id.mitka_list_item_latest_data_text_view);
        TextView nameTextView = view.findViewById(R.id.mitka_info_name_text_view);

        kebab = view.findViewById(R.id.mitka_info_kebab);
        kebab.setOnClickListener(v -> showPopup(v));

        Button beepMitkaButton = view.findViewById(R.id.find_mitka);
        radarButton = view.findViewById(R.id.button_mitka_radar);

        int status = currentMitka.getBluetoothLeService().getConnectionState();
        if (status == 0) {
            statusTextView.setText(ConnectionStatus.DISCONNECTED.toString());

        } else if (status == 1) {
            statusTextView.setText(ConnectionStatus.CONNECTING.toString());
        } else {
            statusTextView.setText(ConnectionStatus.CONNECTED.toString());

        }
        mitkaMelody = view.findViewById(R.id.mitka_list_item_melody_data_text_view);
        if (currentMitka.getMelody() == 0) {
            mitkaMelody.setText(Melody.MELODY1.toString());
        } else {
            mitkaMelody.setText(Melody.values()[currentMitka.getMelody() - 1].toString());
        }

        radarButton.setOnClickListener(v -> {
            Fragment homeMapFragment = new HomeMapFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("mitka", currentMitka);
            homeMapFragment.setArguments(bundle);
            FragmentManager.openFragmentWithBackStack(homeMapFragment, (BaseActivity) getActivity());
        });

        nameTextView.setText(currentMitka.getName());
//        lastSeenTextView.setText("" + currentMitka.getLastSeen().toDate().toString());
        mitkaImageView.setForeground(getResources().getDrawable(currentMitka.getMitkaImage()));
        powerOfSignal.setText(Integer.toString(currentMitka.getSignalPower()));
        macAddressView.setText(currentMitka.getMacAddress());
        beepMitkaButton.setOnClickListener(v -> currentMitka.beep());

        if (currentMitka.getBluetoothLeService() == null) {
            try {
                currentMitka.connect(getContext());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        currentMitka.getBluetoothLeService().readRssi();

        reminderReference = FirebaseFirestore.getInstance().collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getEmail())
                .collection("mitkas")
                .document(currentMitka.getMacAddress())
                .collection("reminders");
        remindersRecyclerView = view.findViewById(R.id.recycler_view_reminders);
        addReminder = view.findViewById(R.id.button_add_reminder);

        reminderReference.get().addOnSuccessListener(queryDocumentSnapshots -> {
                    reminders = queryDocumentSnapshots.isEmpty() ? new ArrayList<>() :
                            queryDocumentSnapshots.toObjects(Reminder.class);
                    oldReminders = queryDocumentSnapshots.isEmpty() ? new ArrayList<>() :
                            queryDocumentSnapshots.toObjects(Reminder.class);

                    RemindersAdapter adapter = new RemindersAdapter(getActivity(), reminders, currentMitka, index);

                    remindersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    remindersRecyclerView.setAdapter(adapter);

                    addReminder.setOnClickListener(v -> {
                        reminders.add(new Reminder(currentMitka.getMacAddress(), RepeatInterval.ONCE,true));
                        adapter.notifyDataSetChanged();
                    });
                }
        ).addOnFailureListener(e -> Log.e(TAG, e.getMessage()));
    }

    public void setRssi(Integer rssi) {
        currentRssi = rssi;
        double signalPower = (((double) currentRssi + (double) 90) * 1.8);
        if (currentRssi != null) {
            powerOfSignal.setText(Integer.toString((int) signalPower));
        }
        if (signalPower > 100) {
            powerOfSignal.setText(getString(R.string.one_hundred));
        } else if (signalPower < 0) {
            powerOfSignal.setText(getString(R.string.zero));
        }
        try {
            Thread.sleep(1000);
            if (currentMitka.getBluetoothLeService().getConnectionState() != BluetoothLeService.STATE_CONNECTED) {
                powerOfSignal.setText(getString(R.string.zero));
            }
            currentMitka.getBluetoothLeService().readRssi();
        } catch (InterruptedException e) {
            Log.e(getString(R.string.error_label), e.getMessage());
        }
    }

    public void setForeGround(ImageView myImageView, ImageView mitkaImageView) {
        Drawable drawable = myImageView.getDrawable();
        mitkaImageView.setForeground(drawable);
    }

    public void showPopup(View view) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.info_kebab, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.edit:
                    Fragment editMitkasFragment = new EditMitkaFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("mitka", currentMitka);
                    bundle.putBoolean("justFound", false);
                    editMitkasFragment.setArguments(bundle);
                    FragmentManager.openFragmentWithBackStack(editMitkasFragment, (BaseActivity) getActivity());
                    return true;
                case R.id.widget:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getContext())) {

                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse(getString(R.string.package_label) + Objects.requireNonNull(getActivity()).getPackageName()));
                        startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
                    } else {
                        Intent widgetIntent = new Intent(getContext(), FloatingViewService.class);
                        if (BaseActivity.getMitkas() != null && BaseActivity.getMitkas().size() > 0) {
                            Mitka mitka = currentMitka;
                            FloatingViewService.setMitka(mitka);
                        }
                        Objects.requireNonNull(getActivity()).startService(widgetIntent);
                        getActivity().finish();
                    }
                    return true;
            }
            return false;
        });
        popup.show();
    }
    private boolean checkReminder(Reminder reminder) {
        if (reminder.isComplete()) {
            for (Reminder secondReminder: oldReminders) {
                if (secondReminder.equals(reminder)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onPause() {
        AtomicInteger uploadCount = new AtomicInteger();
        if (!reminders.equals(oldReminders)) {
            List<Reminder> reminderChanges = new ArrayList<>(Collections2.filter(reminders, this::checkReminder));
            uploadCount.set(reminderChanges.size());
            for (Reminder reminder: reminderChanges) {
                if (reminder.getId() != null) {
                    reminderReference.document(reminder.getId()).set(reminder).addOnSuccessListener(aVoid -> {

                    });
                } else {
                    reminderReference.add(reminder).addOnSuccessListener(documentReference -> {

                    });
                }
            }
        }
        super.onPause();
    }
}
