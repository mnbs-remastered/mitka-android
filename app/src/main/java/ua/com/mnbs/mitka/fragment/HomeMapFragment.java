package ua.com.mnbs.mitka.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.manager.Animations;
import ua.com.mnbs.mitka.model.Mitka;

public class HomeMapFragment extends Fragment {
    private static final String TAG = "HOME_MAP_FRAGMENT";
    private int currentRssi;
    private Mitka mitka;
    private CircleImageView mitkaView;
    private ConstraintLayout layout;
    private boolean firstTime = true;
    private HashMap<Mitka, CircleImageView> views = new HashMap<>();
    private float x = (float) Math.random() * 1000;
    private float y = 570;
    Animation.AnimationListener animL = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mitkaView.setX(x);
            mitkaView.setY(y);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = (ConstraintLayout) inflater.inflate(R.layout.fragment_home_map, null);
        try {
            Mitka mitka = getArguments().getParcelable("mitka");
            mitkaView = new CircleImageView(getActivity());
            mitkaView.setImageDrawable(getActivity().getDrawable(R.drawable.logo_gradient_stroke_svg));
            mitkaView.setBackground(getResources().getDrawable(R.drawable.background_circle_white));
            mitkaView.getId();
            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.height = 100;
            layoutParams.width = 100;
            mitkaView.setLayoutParams(layoutParams);
            //mitkaView.setX((float) Math.random() * 1000);
            //mitkaView.setY(1000);
            Animations anim = new Animations();
            Animation a = anim.fromAtoB(mitkaView.getX(), mitkaView.getY(), x, y, animL, 5000);
            mitkaView.setAnimation(a);
            a.startNow();
            mitka.getBluetoothLeService().readRssi();
//                mitkaView.setOnClickListener(v -> {
//                    try {
//                        mitka.beep();
//                    } catch (IOException e) {
//                        Log.e(TAG, e.getMessage());
//                    }
//                });
            layout.addView(mitkaView);
            views.put(mitka, mitkaView);
            this.mitka = mitka;

        } catch (NullPointerException e) {
            TextView textView = layout.findViewById(R.id.map_empty_view);
            textView.setVisibility(View.VISIBLE);
        }

        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            getActivity().getSupportFragmentManager().popBackStack();
        });

        return layout;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.height = 100;
        layoutParams.width = 100;

    }

    public void setRssi(Integer rssi) {
        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.height = 100;
        layoutParams.width = 100;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            views.forEach((mitka, mitkaView) -> {
                try {
                    mitkaView.setLayoutParams(layoutParams);
                    mitkaView.setX((float) Math.random() * 1000);
                    mitkaView.setY((rssi + 90) * 33);
                    mitka.getBluetoothLeService().readRssi();
                    if (!firstTime) {
                        Thread.sleep(1000);
                    } else {
                        firstTime = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

    }
}
