package ua.com.mnbs.mitka.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.service.FloatingViewService;

public class ExpandedViewListAdapter extends RecyclerView.Adapter<ExpandedViewListAdapter.ViewHolder> {

    private Context contextOfActivity;
    private List<Mitka> listOfMitkas;

    public ExpandedViewListAdapter(Context contextOfActivity, List<Mitka> listOfMitkas) {
        this.contextOfActivity = contextOfActivity;
        this.listOfMitkas = listOfMitkas;
    }

    @NonNull
    @Override
    public ExpandedViewListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(contextOfActivity).inflate(R.layout.item_floating_widget, parent, false);
        return new ExpandedViewListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpandedViewListAdapter.ViewHolder holder, int position) {
        Mitka currentMitka = listOfMitkas.get(position);
        holder.mitkaName.setText(currentMitka.getName());
        holder.mitkaImageView.setImageResource(currentMitka.getMitkaImage());

        holder.mitkaImageView.setOnClickListener(v -> setMitka(position, currentMitka.getMitkaImage()));
    }

    @Override
    public int getItemCount() {
        return listOfMitkas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView mitkaImageView;
        final TextView mitkaName;
        final CardView mitkaLayout;

        ViewHolder(View view) {
            super(view);
            mitkaImageView = view.findViewById(R.id.mitka_image);
            mitkaName = view.findViewById(R.id.mitka_name);
            mitkaLayout = view.findViewById(R.id.mitka_layout);
        }
    }

    public void setMitka(int posOfMitka, int image) {
        Mitka mitka = BaseActivity.getMitkas().get(posOfMitka);
        FloatingViewService.setMitka(mitka);
        FloatingViewService.collapseView(image);
    }
}
