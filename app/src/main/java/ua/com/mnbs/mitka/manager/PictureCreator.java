package ua.com.mnbs.mitka.manager;

import ua.com.mnbs.mitka.R;

public class PictureCreator {

    public static int getPackPicture(int size) {
        switch (size) {
            case 0:
                return R.drawable.pack0;
            case 1:
                return R.drawable.pack1;
            case 2:
                return R.drawable.pack2;
            case 3:
                return R.drawable.pack3;
            case 4:
                return R.drawable.pack4;
            case 5:
                return R.drawable.pack5;
            case 6:
                return R.drawable.pack6;
            case 7:
                return R.drawable.pack7;
            case 8:
                return R.drawable.pack8;
        }
        return R.drawable.pack0;
    }
}
