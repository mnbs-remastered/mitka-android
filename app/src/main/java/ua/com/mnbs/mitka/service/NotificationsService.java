package ua.com.mnbs.mitka.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import androidx.core.app.NotificationCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;

public class NotificationsService extends FirebaseMessagingService {
    public static final int NOTIFICATION_KEY = 29;
    private static final String TAG = "NOTIFICATIONS_SERVICE";
    private static final int FOUND_MITKA_NOTIFICATION_DATA_MAP_SIZE = 3;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            getApplicationContext().getSharedPreferences("token", Context.MODE_PRIVATE).edit().putString("token", s).apply();
        } else {
            FirebaseFirestore.getInstance().collection("users").document(FirebaseAuth.getInstance().getCurrentUser().getEmail())
                    .update("token", s);
        }

    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e(TAG, "Notification Arrived");
        if (remoteMessage.getData().size() < FOUND_MITKA_NOTIFICATION_DATA_MAP_SIZE) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference("mitkas");
            database.goOnline();
            int maxChildren = Integer.parseInt(remoteMessage.getData().get("count"));
            int[] databaseAccessCont = {0};
            dataRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    if (++databaseAccessCont[0] == maxChildren) {
                        getSharedPreferences("update", Context.MODE_PRIVATE).edit().putBoolean("update", false).apply();
                        database.goOffline();
                    }
                    if (dataSnapshot.exists()) {
                        Log.e(TAG, "got mac" + dataSnapshot.getKey());
                    } else
                        Log.e(TAG, "value doesn't exist");
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, databaseError.toString());
                    getSharedPreferences("update", Context.MODE_PRIVATE).edit().clear().apply();
                }
            });
        } else if (remoteMessage.getNotification() != null) {
            showNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getData().get("longitude"), remoteMessage.getData().get("latitude"));
        } else {
            showNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"), remoteMessage.getData().get("longitude"), remoteMessage.getData().get("latitude"));
        }
    }

    @Override
    public void onDeletedMessages() {
    }

    private void showNotification(String title, String body, String longitude, String latitude) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "lost";
        NotificationCompat.Builder notification;

        Intent intent = new Intent(this, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("request code", NOTIFICATION_KEY);
        intent.putExtra("longitude", Double.valueOf(longitude));
        intent.putExtra("latitude", Double.valueOf(latitude));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, NOTIFICATION_KEY, intent, 0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "Mitka!", importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentTitle(title)
                .setContentText(body)
                .setContentIntent(pendingIntent)
                .setContentInfo("info")
                .setSmallIcon(R.mipmap.ic_launcher);
        notificationManager.notify(new Random().nextInt(), notification.build());
    }
}
