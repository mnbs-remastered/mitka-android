package ua.com.mnbs.mitka.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import ua.com.mnbs.mitka.R;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        final Button register = findViewById(R.id.register_button);
        final EditText emailView = findViewById(R.id.register_email);
        final EditText passwordView = findViewById(R.id.login_password);

        register.setOnClickListener(v -> {
            String email = emailView.getText().toString();
            String password = passwordView.getText().toString();
            LinearLayout loginLinearLayout = findViewById(R.id.registration_linear_layout);
            ProgressBar progressBar = findViewById(R.id.progressBar);
            loginLinearLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            if (validatePassword(password))
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password).addOnSuccessListener( authResult ->
                {startActivity(new Intent(this, BaseActivity.class));
                    loginLinearLayout.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);}
                ).addOnFailureListener( e -> {Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    loginLinearLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);});
        });
    }

    private boolean validatePassword(String password) {
        if (password.length() < 6) {
            Toast.makeText(this, getString(R.string.short_password), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

}
