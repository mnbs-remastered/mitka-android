package ua.com.mnbs.mitka.fragment;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.adapter.FoundMitkaListAdapter;
import ua.com.mnbs.mitka.model.Melody;
import ua.com.mnbs.mitka.model.Mitka;

public class EditMitkaFragment extends Fragment {
    private final static String TAG = "EDIT_MITKA_FRAGMENT";
    int index;
    private Button addMitkaButton;
    private CollectionReference firestore;
    private Mitka mitka;
    private EditText mitkaName;
    private TextView mitkaSignal;
    private TextView mitkaMacAddress;
    private ImageView mitkaImageView;
    private Spinner mitkaMelodySpinner;
    private boolean mitkaWasJustFound;
    private List<LinearLayout> constDataContainers;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_edit_mitka_to_add, null);
        addMitkaButton = rootView.findViewById(R.id.mitka_edit_button);
        firestore = FirebaseFirestore.getInstance().collection("users");

        mitka = getArguments().getParcelable(getString(R.string.mitka_label));
        mitkaWasJustFound = getArguments().getBoolean("justFound");
        mitkaImageView = rootView.findViewById(R.id.mitka_img);

        constDataContainers = Arrays.asList(
                rootView.findViewById(R.id.address_container),
                rootView.findViewById(R.id.signal_container),
                rootView.findViewById(R.id.battery_container)
        );

        if (!mitkaWasJustFound) {
            index = BaseActivity.getMitkas().indexOf(mitka);
            addMitkaButton.setText(getContext().getString(R.string.save));
            for (View dataContainer : constDataContainers) {
                dataContainer.setVisibility(View.GONE);
            }
            mitkaImageView.setImageResource(mitka.getMitkaImage());
        }

        mitkaName = rootView.findViewById(R.id.mitka_edit_name);
        mitkaName.setText(mitka.getName());

        mitkaSignal = rootView.findViewById(R.id.mitka_edit_power_text_view);
        mitkaSignal.setText(mitka.getSignalPower() + "");

        mitkaMacAddress = rootView.findViewById(R.id.mitka_edit_mac_address_text_view);
        mitkaMacAddress.setText(mitka.getMacAddress());

        mitka.setMitkaImage(R.drawable.logo);
        mitkaImageView.setOnClickListener(v -> {
            final Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.dialog_choose_icon);
            dialog.setTitle(getString(R.string.icon_choosing));
            dialog.show();

            Map<Integer, ImageView> images = new HashMap<>();

            images.put(R.drawable.icon_backpack, dialog.findViewById(R.id.backpack));
            images.put(R.drawable.icon_car_key, dialog.findViewById(R.id.car_key));
            images.put(R.drawable.icon_door_key, dialog.findViewById(R.id.door_key));
            images.put(R.drawable.icon_key, dialog.findViewById(R.id.key));
            images.put(R.drawable.icon_umbrella, dialog.findViewById(R.id.umbrella));
            images.put(R.drawable.icon_wallet, dialog.findViewById(R.id.wallet));
            images.put(R.drawable.icon_portfolio, dialog.findViewById(R.id.portfolio));
            images.put(R.drawable.icon_smartphone, dialog.findViewById(R.id.smartphone));
            images.put(R.drawable.icon_laptop, dialog.findViewById(R.id.laptop));

            for (Map.Entry<Integer, ImageView> entry : images.entrySet()) {
                ImageView imageView = entry.getValue();

                imageView.setOnClickListener(v1 -> {
                            Drawable myImageDrawable = ContextCompat.getDrawable(getContext(), entry.getKey());
                            mitkaImageView.setForeground(myImageDrawable);
                            mitka.setMitkaImage(entry.getKey());
                            dialog.hide();
                        }
                );
            }
        });

        ArrayAdapter<Melody> melodyAdapter = new ArrayAdapter<>(
                getContext(), R.layout.spinner_item, Melody.values());
        mitkaMelodySpinner = rootView.findViewById(R.id.mitka_edit_melody_spinner);
        mitkaMelodySpinner.setAdapter(melodyAdapter);
        setMelodyIntoSpinner(mitka.getMelody());
        mitkaMelodySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mitka.setMelody(position + 1);

            }


            @Override

            public void onNothingSelected(AdapterView<?> parent) {


            }

        });

        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            getActivity().getSupportFragmentManager().popBackStack();
        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addMitkaButton.setOnClickListener(v -> sendMitkaToFirestore());
    }

    private void sendMitkaToFirestore() {
        mitka.setName(mitkaName.getText().toString());
        mitka.setLastSeen(Timestamp.now());
        mitka.setMelody(mitkaMelodySpinner.getSelectedItemPosition() + 1);
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        DocumentReference mitkaRef = firestore.document(email)
                .collection("mitkas")
                .document(mitka.getMacAddress());
        if (mitkaWasJustFound) {
            mitkaRef.set(mitka.toHashMap())
                    .addOnSuccessListener(documentReference -> {
                        BaseActivity.addMitka(mitka);
                        BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.navigation);
                        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
                    });
        } else {
            mitkaRef.update("melody", mitka.getMelody(), "mitkaImage", mitka.getMitkaImage(),
                    "name", mitka.getName())
                    .addOnSuccessListener(aVoid -> {
                        BaseActivity.updateMitkaImage(index, mitka);
                        BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.navigation);
                        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
                    }).addOnFailureListener(e -> Log.e(TAG, e.getMessage()));
        }
    }

    private void setMelodyIntoSpinner(int melody) {
        if (melody == 0) {
            mitkaMelodySpinner.setSelection(0);
        } else {
            mitkaMelodySpinner.setSelection(melody - 1);
        }
    }
}
