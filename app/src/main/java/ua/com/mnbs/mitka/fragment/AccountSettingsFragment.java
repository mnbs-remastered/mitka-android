package ua.com.mnbs.mitka.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.FirebaseFirestore;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.activity.LoginActivity;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.service.FloatingViewService;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class AccountSettingsFragment extends Fragment {
    private static final String TAG = "AccountSettingsFragment";
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    private TextView emailView;
    private Button floatingWidget;
    private Button exitButton;
    private Button chooseMelody;
    private Button changePassword;
    private boolean changePasswordPossible = true;
    private Switch enableRemindersSwitch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_account_settings, null);
        emailView = rootView.findViewById(R.id.settings_email);
        floatingWidget = rootView.findViewById(R.id.notify_me);
        exitButton = rootView.findViewById(R.id.button_exit);
        chooseMelody = rootView.findViewById(R.id.select_melody);
        changePassword = rootView.findViewById(R.id.change_password_button);
        enableRemindersSwitch = rootView.findViewById(R.id.switch_enable_all_reminders);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email = user.getEmail();
        emailView.setText(email);
        for (UserInfo userInfo :user.getProviderData()) {
            if (userInfo.getProviderId().equals("google.com")) {
                changePasswordPossible = false;
            }
        }
        if (changePasswordPossible) {
            View dialogView = inflater.inflate(R.layout.dialog_change_password, null);
            EditText passwordView = dialogView.findViewById(R.id.password_new);
            EditText oldPasswordView = dialogView.findViewById(R.id.password_old);
            EditText repeatPasswordView = dialogView.findViewById(R.id.password_new_repeat);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(dialogView)
                    .setPositiveButton("Підтвердити", (dialog, which) -> {
                        String password = passwordView.getText().toString();
                        String oldPassword = oldPasswordView.getText().toString();
                        String repeatPassword = repeatPasswordView.getText().toString();
                        if (password.isEmpty() || oldPassword.isEmpty()) {
                            Toast.makeText(getActivity(), "Паролі не можуть бути пустими", Toast.LENGTH_LONG).show();
                        } else if (password.equals(repeatPassword)) {
                            FirebaseAuth.getInstance().signInWithEmailAndPassword(user.getEmail(), oldPassword).addOnSuccessListener(authResult ->
                                    user.updatePassword(password)
                                            .addOnFailureListener(e -> Log.e(TAG, e.getLocalizedMessage()))
                                            .addOnSuccessListener(authResult2 ->
                                                    Toast.makeText(getActivity(), "Пароль змінено!", Toast.LENGTH_LONG).show()
                                            )
                            ).addOnFailureListener(e -> Log.e(TAG, e.getLocalizedMessage()));
                        } else {
                            Toast.makeText(getActivity(), "Паролі не збігаються", Toast.LENGTH_LONG).show();
                        }
                    })
                    .setNegativeButton("Скасувати", (dialog, which) -> dialog.cancel());

            changePassword.setOnClickListener(v -> {
                if (dialogView.getParent() != null) {
                    ((ViewGroup) dialogView.getParent()).removeView(dialogView);
                    passwordView.setText("");
                    oldPasswordView.setText("");
                    repeatPasswordView.setText("");
                }
                builder.create().show();
            });
        } else {
            changePassword.setVisibility(View.GONE);
        }

        SharedPreferences preferences = getActivity().getSharedPreferences("reminders", Context.MODE_PRIVATE);
        enableRemindersSwitch.setChecked(preferences.getBoolean("enabled", true));
        enableRemindersSwitch.setOnCheckedChangeListener((buttonView, isChecked) ->
                preferences.edit().putBoolean("enabled", isChecked).apply());
        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(null);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        floatingWidget.setOnClickListener(View -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getContext())) {

                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse(getString(R.string.package_label) + Objects.requireNonNull(getActivity()).getPackageName()));
                startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
            } else {
                Intent widgetIntent = new Intent(getContext(), FloatingViewService.class);
                if (BaseActivity.getMitkas() != null && BaseActivity.getMitkas().size() > 0) {
                    Mitka mitka = BaseActivity.getMitkas().get(1);
                    FloatingViewService.setMitka(mitka);
                }
                Objects.requireNonNull(getActivity()).startService(widgetIntent);
                getActivity().finish();
            }
        });
        exitButton.setOnClickListener(event -> {
            FirebaseAuth.getInstance().signOut();
            Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
            getActivity().getSharedPreferences(getString(R.string.mitkas_label), Context.MODE_PRIVATE).edit().clear().apply();
            FirebaseFirestore.getInstance().clearPersistence().addOnCompleteListener(task -> startActivity(loginIntent));
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION) {
                if (resultCode == RESULT_OK) {
                    floatingWidget.setOnClickListener(view -> {
                        Intent widgetIntent = new Intent(getContext(), FloatingViewService.class);
                        if (BaseActivity.getMitkas() != null && BaseActivity.getMitkas().size() > 0) {
                            widgetIntent.putExtra(getString(R.string.mitka_label), (Serializable) BaseActivity.getMitkas().get(0));
                        }
                        getActivity().startService(widgetIntent);
                        getActivity().finish();
                    });
                } else {
                    getActivity().finish();
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
