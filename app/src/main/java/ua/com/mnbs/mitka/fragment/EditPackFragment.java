package ua.com.mnbs.mitka.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.common.collect.Collections2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.adapter.MitkasPackAdapter;
import ua.com.mnbs.mitka.adapter.NewMitkaInPackAdapter;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.manager.PictureCreator;
import ua.com.mnbs.mitka.model.Melody;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.model.Pack;

public class EditPackFragment extends Fragment {

    private RecyclerView addedMitkasView;
    private RecyclerView allMitkasView;

    private NewMitkaInPackAdapter allMitkasAdapter;
    private MitkasPackAdapter addedMitkasAdapter;
    private EditText packNameEditText;
    private ImageButton addMitkasButton;
    private ImageView packIcon;
    private Button submitButton;
    private Spinner melodySpinner;

    private List<Mitka> addedMitkas;
    private List<Mitka> allMitkas;
    private Pack pack;

    private boolean packJustCreated;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_pack, null);

        packJustCreated = getArguments().getBoolean("justCreated");

        allMitkas = BaseActivity.getMitkas() == null ? new ArrayList<>() :
                BaseActivity.getMitkas().subList(1, BaseActivity.getMitkas().size());

        packNameEditText = rootView.findViewById(R.id.pack_name);
        submitButton = rootView.findViewById(R.id.submit_pack_button);
        packIcon = rootView.findViewById(R.id.pack_icon);

        if (packJustCreated) {
            pack = new Pack();
            addedMitkas = new ArrayList<>();

            submitButton.setText(R.string.add);
        } else {
            pack = getArguments().getParcelable("pack");
            addedMitkas = new ArrayList<>(Collections2.filter(BaseActivity.getMitkas(),
                    input -> pack.getMitkas().contains(input.getMacAddress())));
            submitButton.setText(R.string.save);
            packIcon.setImageResource(PictureCreator.getPackPicture(addedMitkas.size()));

            packNameEditText.setText(pack.getName());
        }

        addedMitkasView = rootView.findViewById(R.id.added_mitkas);
        addMitkasButton = rootView.findViewById(R.id.button_add_mitkas);
        addedMitkasView.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        addedMitkasAdapter = new MitkasPackAdapter(getActivity(), addedMitkas, true);
        addedMitkasView.setAdapter(addedMitkasAdapter);

        allMitkasView = inflater.inflate(R.layout.dialog_choose_mitkas, null).findViewById(R.id.add_mitkas_recyclerView);


        allMitkasView.setLayoutManager(new LinearLayoutManager(getActivity()));
        allMitkasAdapter = new NewMitkaInPackAdapter(getActivity(), allMitkas, addedMitkas, addedMitkasAdapter);
        allMitkasView.setAdapter(allMitkasAdapter);

        melodySpinner = rootView.findViewById(R.id.pack_edit_melody_spiner);
        ArrayAdapter<Melody> melodyArrayAdapter = new ArrayAdapter<>(
                getContext(), R.layout.spinner_item, Melody.values());
        melodySpinner.setAdapter(melodyArrayAdapter);
        setMelodyIntoSpinner(pack.getMelody());

        submitButton.setOnClickListener(v -> {
            pack.setName(packNameEditText.getText().toString());
            pack.setMelody(melodySpinner.getSelectedItemPosition() + 1);
            addedMitkas.remove(0);
            pack.setMitkas(new ArrayList<>(Collections2.transform(addedMitkas, Mitka::getMacAddress)));
            FirebaseFirestore.getInstance()
                    .collection("users")
                    .document(FirebaseAuth.getInstance().getCurrentUser().getEmail())
                    .collection("packs")
                    .document(pack.getName())
                    .set(pack)
                    .addOnCompleteListener(aVoid ->
                            FragmentManager.openFragment(new PackFragment(), (BaseActivity) getActivity())
                    ).addOnFailureListener(e -> Log.e("NEWPACKFRAGMENT:", e.getMessage()));
        });

        addMitkasButton.setOnClickListener(v -> {
            if (allMitkasView.getParent() != null) {
                ((ViewGroup) allMitkasView.getParent()).removeView(allMitkasView);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            AlertDialog dialog = builder.setTitle(getContext().getString(R.string.choose_mitkas))
                    .setView(allMitkasView)
                    .setPositiveButton(getContext().getString(R.string.add),
                            (dialog1, which) -> dialog1.dismiss())
                    .setOnCancelListener(DialogInterface::dismiss)
                    .setOnDismissListener(dialog1 -> packIcon.setImageResource(
                            PictureCreator.getPackPicture(addedMitkas.size() - 1)))
                    .create();
            dialog.show();
        });
        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            getActivity().getSupportFragmentManager().popBackStack();
        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setMelodyIntoSpinner(int melody) {
        if (melody == 0) {
            melodySpinner.setSelection(0);
        } else {
            melodySpinner.setSelection(melody - 1);
        }
    }
}
