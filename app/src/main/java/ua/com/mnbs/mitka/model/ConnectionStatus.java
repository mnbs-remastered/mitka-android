package ua.com.mnbs.mitka.model;

public enum ConnectionStatus {

    CONNECTED("З'єднано"),
    CONNECTING("З'єднання"),
    DISCONNECTED("Роз'єднано");

    private String text;

    ConnectionStatus(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String toString() {
        return text;
    }
}
