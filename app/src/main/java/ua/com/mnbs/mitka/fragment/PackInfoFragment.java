package ua.com.mnbs.mitka.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.common.collect.Collections2;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ua.com.mnbs.mitka.R;
import ua.com.mnbs.mitka.activity.BaseActivity;
import ua.com.mnbs.mitka.adapter.MitkasPackAdapter;
import ua.com.mnbs.mitka.adapter.RemindersAdapter;
import ua.com.mnbs.mitka.manager.FragmentManager;
import ua.com.mnbs.mitka.manager.PictureCreator;
import ua.com.mnbs.mitka.model.Melody;
import ua.com.mnbs.mitka.model.Mitka;
import ua.com.mnbs.mitka.model.Pack;
import ua.com.mnbs.mitka.model.Reminder;
import ua.com.mnbs.mitka.model.RepeatInterval;
import ua.com.mnbs.mitka.service.FloatingViewService;

public class PackInfoFragment extends Fragment {
    private final static String TAG = "PACK_INFO";
    private static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    private TextView nameTextView;
    private RecyclerView mitkasRecyclerView;
    private Button checkPackButton;
    private ImageButton kebab;
    private Button addReminder;
    private RecyclerView remindersRecyclerView;
    private List<Reminder> reminders;
    private List<Reminder> oldReminders;
    private List<Mitka> mitkas;
    private CollectionReference reminderReference;
    private ImageView packIcon;
    private TextView gatheredMitkasView;
    private TextView melodyTextView;
    int index;

    private Pack pack;

    public PackInfoFragment(Pack pack, int index) {
        this.pack = pack;
        this.index = index;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pack_info, null);

        nameTextView = rootView.findViewById(R.id.pack_name_text_view);
        mitkasRecyclerView = rootView.findViewById(R.id.pack_mitkas_list);
        checkPackButton = rootView.findViewById(R.id.pack_check_button);
        addReminder = rootView.findViewById(R.id.button_add_reminder);
        remindersRecyclerView = rootView.findViewById(R.id.recycler_view_reminders);
        melodyTextView = rootView.findViewById(R.id.melody_text_view);
        kebab = rootView.findViewById(R.id.pack_info_kebab);

        packIcon = rootView.findViewById(R.id.pack_icon);
        gatheredMitkasView = rootView.findViewById(R.id.gathered_mitkas);

        packIcon.setImageResource(PictureCreator.getPackPicture(pack.getMitkas().size()));
        gatheredMitkasView.setText(String.format("%d / %d", 0, pack.getMitkas().size()));

        mitkas = new ArrayList<>(Collections2.filter(BaseActivity.getMitkas(),
                input -> pack.getMitkas().contains(input.getMacAddress())));

        MitkasPackAdapter mitkasPackAdapter = new MitkasPackAdapter(getActivity(), mitkas, false);
        mitkasRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        mitkasRecyclerView.setAdapter(mitkasPackAdapter);
        nameTextView.setText(pack.getName());

        if (pack.getMelody() == 0) {
            melodyTextView.setText(Melody.MELODY1.toString());
        } else {
            melodyTextView.setText(Melody.values()[pack.getMelody() - 1].toString());
        }

        reminderReference = FirebaseFirestore.getInstance().collection("users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getEmail())
                .collection("packs")
                .document(pack.getName())
                .collection("reminders");

        reminderReference.get().addOnSuccessListener(queryDocumentSnapshots -> {
                    reminders = queryDocumentSnapshots.isEmpty() ? new ArrayList<>() :
                            queryDocumentSnapshots.toObjects(Reminder.class);
                    oldReminders = queryDocumentSnapshots.isEmpty() ? new ArrayList<>() :
                            queryDocumentSnapshots.toObjects(Reminder.class);

                    RemindersAdapter adapter = new RemindersAdapter(getActivity(), reminders, pack, index);

                    remindersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

                    remindersRecyclerView.setAdapter(adapter);

                    addReminder.setOnClickListener(v -> {
                        reminders.add(new Reminder(pack.getName(), RepeatInterval.ONCE,true));
                        adapter.notifyDataSetChanged();
                    });
                }
        ).addOnFailureListener(e -> Log.e(TAG, e.getMessage()));

        checkPackButton.setOnClickListener(v -> {
            for (Mitka mitka : mitkas) {
                mitka.beepWithMelody(pack.getMelody() == 0 ? 1 : pack.getMelody());
            }
        });

        kebab.setOnClickListener(this::showPopup);

        Toolbar toolbar = ((BaseActivity) getActivity()).getToolbar();
        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setNavigationOnClickListener(v -> {
            BottomNavigationView mBottomNavigationView = getActivity().findViewById(R.id.navigation);
            mBottomNavigationView.setSelectedItemId(R.id.navigation_pack);
            toolbar.setNavigationIcon(null);
        });
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private boolean checkReminder(Reminder reminder) {
        if (reminder.isComplete()) {
            for (Reminder secondReminder: oldReminders) {
                if (secondReminder.equals(reminder)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onPause() {
        AtomicInteger uploadCount = new AtomicInteger();
        if (!reminders.equals(oldReminders)) {
            List<Reminder> reminderChanges = new ArrayList<>(Collections2.filter(reminders, this::checkReminder));
            uploadCount.set(reminderChanges.size());
            for (Reminder reminder: reminderChanges) {
                if (reminder.getId() != null) {
                    reminderReference.document(reminder.getId()).set(reminder).addOnSuccessListener(aVoid -> {

                    });
                } else {
                    reminderReference.add(reminder).addOnSuccessListener(documentReference -> {

                    });
                }
            }
        }
        super.onPause();
    }

    public void showPopup(View view) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.info_kebab, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.edit:
                    EditPackFragment editPackFragment = new EditPackFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("justCreated", false);
                    bundle.putParcelable("pack", pack);
                    editPackFragment.setArguments(bundle);
                    FragmentManager.openFragmentWithBackStack(editPackFragment, (BaseActivity) getActivity());
                    return true;
                case R.id.widget:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getContext())) {

                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse(getString(R.string.package_label) + Objects.requireNonNull(getActivity()).getPackageName()));
                        startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION);
                    } else {
                        Intent widgetIntent = new Intent(getContext(), FloatingViewService.class);
                        if (BaseActivity.getMitkas() != null && BaseActivity.getMitkas().size() > 0) {
                            FloatingViewService.setListMitka(mitkas.subList(0, mitkas.size()));
                        }
                        Objects.requireNonNull(getActivity()).startService(widgetIntent);
                        getActivity().finish();
                    }
                    return true;
            }
            return false;
        });
        popup.show();
    }
}
